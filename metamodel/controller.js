var Controller, exports, ref;
var associated_cids = [];
var associated_name_cids = [];

Controller = class Controller {

  constructor() {
    this.connection = new MetamodelConnector();
  }

  // This function reuses exportJSON of ORM 2 and removes model
  exportJSON_without_modal(){
  	return createJSON();
  }

  beautify(data) {
    var pretty = JSON.stringify(data, undefined, 4);
    console.log(pretty);
  }

  getConnection() {
    return this.connection;
  }

  saveFile(data){
    mostrarLoading();
    var pretty = JSON.stringify(data, undefined, 4);
    var $link = $("<a/>");
    var text = encodeURIComponent( pretty );

    $link
    .attr( "download", "myModel.json" )
    .attr( "href", "data:application/octet-stream," + text )
    .appendTo( "body" )
    .get(0)
    .click();
    closeLoading();
  }

  toMetamodel() {
      var json = this.exportJSON_without_modal();
      return this.connection.request_orm2meta(json, this.saveFile);
  }

  toUML() {
      var json = this.exportJSON_without_modal();
      this.connection.request_orm2uml(json, this.saveFile);
      this.redirect_UML_editor();
  }

  toORM(content){
    this.connection.request_uml2orm(content, this.importJSON);
  }

  importJSON(data){
    var obj = JSON.parse(JSON.stringify(data, undefined, 4));
    console.log(obj);
    graphJson_Entities(obj.entities);
    graphJson_Relationships(obj.relationships);
    graphJson_Connectors(obj.connectors);
  }

  loadUML(btn){
  	if(document.getElementById('FileInput')){
  		mostrarLoading();
  		var btn_submit = document.getElementById('FileInput');
  		btn_submit.click();
  		document.getElementById('FileInput').addEventListener('change', (event) => {
        var file = event.target.files[0];
        this.readFile(file);
      });
  		$(btn).unbind('click');
  		closeLoading();
  	}
  }

  readFile(file){
    var reader = new FileReader();

    reader.onload = () => {
      this.toORM(reader.result);
    };
    reader.readAsText(file);
  }

  redirect_UML_editor(){
    var url_r = "http://crowd.fi.uncoma.edu.ar/wicom-qdod/web-src/model_editor.php?type=UML";
    window.open(url_r, "_blank");
  }

}

exports = exports != null ? exports : this;
exports.metamodel = (ref = exports.metamodel) != null ? ref : this;
exports.metamodel.Controller.intialise = function() {
  return exports.metamodel.Controller = new Controller();
};
