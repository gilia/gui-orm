$(document).ready(function () {
    $('#toKF').click(function () {
      var contr = new Controller;
      contr.toMetamodel();
    });

    $('#toUML').click(function () {
      var contr = new Controller;
      contr.toUML();
    });

    $('#loadUML').click(function () {
      var contr = new Controller;
      contr.loadUML($(this));
    });
});

function load(btn){
  var contr = new Controller;
  contr.loadUML(btn);
}
