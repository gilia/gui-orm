var MetamodelConnector, ref, exports;

MetamodelConnector = class MetamodelConnector {
  // @param error_callback {function} which error callback function to use.
  constructor(error_callback) {
    this.error_callback = error_callback;
    this.urlprefix = "http://localhost:8080/";
  }

  set_urlprefix(str) {
    return this.urlprefix = str;
  }

	request_orm2meta(json, callback_function) {
		var postdata, url;
		url = this.urlprefix + "ormtometa";
		return $.ajax({
      type: "POST",
			headers: {
				"Content-Type": "application/json"
			},
      url: url,
      data: json,
      success: callback_function,
      error: this.error_callback
    });
	}

  request_uml2orm(json, callback_function) {
    var postdata, url;
    url = this.urlprefix + "umltoorm";
    return $.ajax({
      type: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      url: url,
      data: json,
      success: callback_function,
      error: this.error_callback
    });
  }



	request_orm2uml(json, callback_function) {
		var postdata, url;
		url = this.urlprefix + "ormtouml";
		return $.ajax({
			type: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			url: url,
			data: json,
			success: callback_function,
			error: this.error_callback
		});
	}

	request_meta2uml(json_meta, callback_function) {
		var postdata, url;
		url = this.urlprefix + "metatouml";
		return $.ajax({
			type: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			url: url,
			data: json_meta,
			success: callback_function,
			error: this.error_callback
		});
	}

	request_meta2eer(json_meta, callback_function) {
		var postdata, url;
		url = this.urlprefix + "metatoeer";
		return $.ajax({
			type: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			url: url,
			data: json_meta,
			success: callback_function,
			error: this.error_callback
		});
	}

}

exports = exports != null ? exports : this;
exports.metamodel = (ref = exports.metamodel) != null ? ref : this;
exports.metamodel.MetamodelConnector.intialise = function() {
  return exports.metamodel.MetamodelConnector = new MetamodelConnector();
};
