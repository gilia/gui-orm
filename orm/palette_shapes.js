//CREATE SHAPES
var uml = joint.shapes.uml;
var erd = joint.shapes.erd;
var orm = joint.shapes.orm;
var gridSize = $('#l_w').width();

var roleUnary_shape = new orm.Role_1({
	attrs:{'role_name': {text: 'role_name',fontSize: 12,y: 25,},
		   'oto': {'x1':2,'y1':-4 ,'x2':15,'y2':-4,strokeWidth: 2}   
		  }
});  

var roleBinary_shape = new orm.Role_2({
	attrs:{'role_name': {text: 'role_name',fontSize: 12,y: 25,},
		   'oto': {'x1':2,'y1':-4 ,'x2':15,'y2':-4,strokeWidth: 2},
		   'line_1': {'x2':0,'y2':10,strokeWidth: 2}						
		  }
});

var entity_shape = new orm.Entity({
	 attrs: {
		body: {
            rx:5,ry:5,
		},
        label: {
			fontSize: 12,            
	 }}
});

var entityRefMode_shape = new orm.EntityRefMode({
	 attrs: {
		body: {
            rx:5,ry:5,
		},
        label: {
			fontSize: 12,            
		 },
        refMode: {
			fontSize: 10,            
	    }
		}
});

var entity_value = new orm.Value({
	 attrs: {
		body: {
            rx:5,ry:5,
		},
        label: {
			fontSize: 12,            
	    }}
});
			
var union= new orm.Union({
	
	 attrs: {
		external_ellipse: {
			cx:0,
			cy:0,
			ry:12,
			rx:12,
		},
        internal_ellipse: {
			cx:0,
			cy:0,
			ry:6,
			rx:6,           
	    }}
		
});		

var exlusiveExhaustive= new orm.ExclusiveExhaustive({
	attrs: {
		external_ellipse: {
			cx:0,
			cy:0,
			ry:12,
			rx:12,
		},
        internal_ellipse: {
			cx:0,
			cy:0,
			ry:6,
			rx:6,           
	    },
	line1: {
			x2:8.98111,
			y2:8.92465,
			y1:-8.71622,
			x1:-8.85748,
		},
	line2: {
			x2:8.98111,
			y2:-8.71622,
			y1:8.71622,
			x1:-8.85748,
			
        }
		}
		
});		

var exclusive= new orm.Exclusive({
	attrs: {
		external_ellipse: {
			cx:0,
			cy:0,
			ry:12,
			rx:12,
		},
        
	line1: {
			x2:8.98111,
			y2:8.92465,
			y1:-8.71622,
			x1:-8.85748,
		},
	line2: {
			x2:8.98111,
			y2:-8.71622,
			y1:8.71622,
			x1:-8.85748,
			
        }
	}
		
});		

var equality= new orm.Equality({
	attrs: {
		external_ellipse: {
			cx:0,
			cy:0,
			ry:12,
			rx:12,
		},
		line1: {
			x2:-6,
			y2:-3,
			y1:-3,
			x1:6,
		},
		line2: {
			x2:-6,
			y2:3,
			y1:3,
			x1:6,
		}
	}
		
});		

var subset= new orm.Subset({
	attrs: {
		external_ellipse: {
			cx:0,
			cy:0,
			ry:12,
			rx:12,
		},
		simb: {
			text: 'U',
            'font-family': 'Arial', 
			fill:original_strokeFill_constraint,
			stroke:original_strokeFill_constraint,
			'stroke-width':0,
			x:-7,
			y:3,
			fontSize:18,
			'text-anchor':'start',
			transform:'rotate(90 -1.8749999999999998,-2.5000000000000004)'
			
			
        },
		line2: {
			x2:-6,
			y2:6,
			y1:6,
			x1:6,
		}
	}
		
});	

//REDIMENSIONO LOS ELEMENTOS PARA QUE SE VEAN MAS PEQUEÑOS DENTRO DE LA PALETA.
entity_shape.scale(0.7,0.7);
entityRefMode_shape.scale(0.7,0.7);
entity_value.scale(0.7,0.7);
roleUnary_shape.scale(0.6,0.6);
roleBinary_shape.scale(0.6,0.6);

function paletteElements() {
	return [entity_shape, entityRefMode_shape, entity_value,roleUnary_shape,roleBinary_shape,union,exclusive,exlusiveExhaustive,subset,equality];
}

function paletteElementsReduced() {
	return [entity_shape, entityRefMode_shape, entity_value,roleUnary_shape,roleBinary_shape,union,exclusive,exlusiveExhaustive,subset,equality];
}

function sortPalette(horizontal, size, elements) {
	var col = 0;
	var row = 0;
	var x_postition=0;
	var y_postition=0;
	var new_point;
	
	//var elements = paletteElements();
	elements.forEach(function (element) {
		
		//alert(element.prop('type'));
		x_postition= (gridSize * col + gridSize / 2 - element.attributes.size.width / 2)-4;
		y_postition= 55 * row + 55 / 2 - element.attributes.size.height / 2;
		element.position(x_postition,y_postition);
		
		if (horizontal) {
			if (col < size - 1) {
				col++
			} else {
				col = 0;
				row++;
			}
		} else {
			if (row < size - 1) {
				row++
			} else {
				row = 0;
				col++;
			}
		}
		
	});
}
