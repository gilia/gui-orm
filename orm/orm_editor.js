var ctrl_down=false;
var cloning_element=false;
var linking_roles=false;
var waiting_ISA_element=false;
var waiting_Role_constraint=false;
var waiting_ISA_constraint=false;
var elementos_seleccionados=[];
var selected_roles=[];
var cantidad_elementos_seleccionar=0;
var estado_actual=0;
var esprando_seleccion=false;

//TOOLBAR OF ELEMENTS SELECTED IN THE PAPER.
//DIFFERENT BUTTONS ARE SHOWN ACCORDING TO THE ELEMENT TYPE
var tools = $('<div class="toolbar" ></div>');
tools.append('<div id="elementDeleteButton" class="tools tools-delete ot_elto" onclick="elementDelete()"><a class="tooltips" href="#"><i class="material-icons">delete_forever</i><span>Remove the element</span></a></div>');
tools.append('<div id="elementISA" class="tools tools-elementISA ot_elto" style="padging-left:10px;" onmousedown="showIsaPosibleConnections()"><a class="tooltips" href="#"><i class="material-icons">expand_less</i><span>ISA</span></a></div>');
tools.append('<div id="elementDuplicateButton" class="tools tools-duplicate ot_elto" onclick="elementDuplicate()"><a class="tooltips" href="#"><i class="material-icons">file_copy</i><span>Duplicate the element</span></a></div>');
tools.append('<div id="elementUnLinkButton" class="tools tools-code ot_elto" onclick="elementUnLink()"><a class="tooltips" href="#"><i class="material-icons">link_off</i><span>Unlink element</span></a></div>');
tools.append('<div id="elementLinkButton" class="tools tools-repeat ot_elto" onclick="linkElementType()"><a class="tooltips" href="#"><i class="material-icons">repeat</i><span>Link element</span></a></div>');
tools.append('<div id="elementRoleConstraint" class="tools tools-role-constraint ot_elto" onclick="roleConstraint(event)"><a class="tooltips" href="#"><i class="material-icons">compare_arrows</i><span>Role Constraint</span></a></div>');
tools.append('<div id="elementISAConstraint" class="tools tools-isa-constraint ot_elto" onclick="isaConstraint(event)"><a class="tooltips" href="#"><i class="material-icons">call_split</i><span>ISA Constraint</span></a></div>');
tools.css({display: 'none'});

var highlighted1 = false;
var highlighted2 = false;

//SET A DIFERENT HIGHLIGHTER.
//myHighlighter1 - TIPICAL SELECTION -  SET ELEMENT COLOR
//myHighlighter2 - EXTERNAL CONTOUR
//myHighlighter3 - POSSIBLE SELECTORS

var myHighlighter1 = {
  highlighter: {
	name: 'addClass',
	options: {
	  className: 'highlighted'
	}
  }
}

var myHighlighter2 = {
	highlighter: {

	  name: 'stroke',
	  options: {
			padding: 15	,
			rx: 1,
			ry: 1,
			attrs: {
				'stroke-width': 0.8,
				'stroke-dasharray': '5,5',
				stroke: '#454545',
				z:-100
			}
		}
	}
}

var myHighlighterWrong = {
  highlighter: {
	name: 'addClass',
	options: {
	  className: 'highlightedWrong'
	}
  }
}

var myHighlighter3 = {
  highlighter: {
	name: 'addClass',
	options: {
	  className: 'highlighted_pos'
	}
  }
}

var paletteGraph = new joint.dia.Graph();
//PALETTE GRAPH
var palette = new joint.dia.Paper({
  el: document.getElementById('l_w'),
  width: 100,
  height: 1000,
  model: paletteGraph,
  interactive: false
});

var paletteElements = paletteElements();
var paletteElementsReduced = paletteElementsReduced();
var actualElements = paletteElementsReduced;

sortPalette(false, 10, paletteElements);
paletteGraph.addCells(actualElements);

//DRAG & DROP DE LA PALETA
palette.on('cell:pointerdown', function (cellView, e, x, y) {
	unSelectAll();
	//alert('CELL POINTER DOWN ELEMENT NESTED IN TOOL PALETTE');
	$('body').append('<div id="flyPaper"></div>');
	var flyGraph = new joint.dia.Graph,
	flyPaper = new joint.dia.Paper({
		el: $('#flyPaper'),
		model: flyGraph,
		interactive: false,
	});
	//EN VEZ DE CLONAR EL OBJETO CREO UNO NUEVO CON LOS TAMAÑOS REALES.
	flyShape = cloneElement(cellView.model.prop('type'));


	pos = cellView.model.position();
	offset = {
		x: flyShape.attributes.size.width / 2 * paper.scale().sx,
		y: flyShape.attributes.size.height / 2 * paper.scale().sy
	};
	flyPaper.scale(paper.scale().sx);
	flyShape.position(2, 2);

	flyGraph.addCell(flyShape);
	$("#flyPaper").offset({
		left: (e.pageX - offset.x),
		top: (e.pageY - offset.y)
	});
	$('body').on('mousemove.fly', function (e) {
		$("#flyPaper").offset({
			left: (e.pageX - offset.x),
			top: (e.pageY - offset.y)
		});
	});
	$('body').on('mouseup.fly', function (e) {
		var x = e.pageX,
		y = e.pageY,
		target = paper.$el.offset();
		origin = palette.$el.offset();
		// Dropped over paper and not over origin
		if ((x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) &&
			!(x > origin.left && x < origin.left + palette.$el.width() && y > origin.top && y < origin.top + palette.$el.height())) {
			var newElement = flyShape.clone();
			var relativePoint = paper.clientToLocalPoint(e.clientX, e.clientY);
				newElement.position((relativePoint.x - (newElement.attributes.size.width / 2)+2), (relativePoint.y - (newElement.attributes.size.height / 2)+2));
				graph.addCell(newElement);
				var cv=newElement.findView(paper);

				selectElement(cv);
				showAtributes(cv);
		}
		$('body').off('mousemove.fly').off('mouseup.fly');
		flyShape.remove();
		$('#flyPaper').remove();
	});
});

var graph = new joint.dia.Graph();
var paper = new joint.dia.Paper({
	el: document.getElementById('r_w'),
	model: graph,
	width: 500,
	height: 500,
	gridSize: 5,
	drawGrid: false,
	markAvailable: true,
	background: {
		color: 'white'
	},
	interactive: function(cellView) {
		if (cellView.model instanceof joint.dia.Link) {
		// Disable the default vertex add functionality on pointerdown.
			return { vertexAdd: false };
		}
		return true;
	}
});

//DBLCLICK OVER CELL, MAKES SELECTION STYLE AND SHOWS PROPERTIES ACCORDING TO TYPE CELL
paper.on('cell:pointerdblclick', function(cellView, evt, x, y) {
	cellView.highlight(null, myHighlighter2);
	cellView.highlight(null, myHighlighter1);
	cellView.model.toFront();
	showElementToolsElement(cellView);
	showAtributes(cellView);
})

//AVOID UNSELECT ISA ELEMENTS PREVIOUS SELECTED
paper.on('blank:pointerdown', function(evt, x, y) {
	if(!waiting_ISA_element)
	{
		unSelectAll();
		if(!cloning_element)
			hideAtributes();
	}
});

//HIDE ALL TYPES OF CSS SELECTIONS WHILE MOVE ELEMENTE
paper.on('element:pointermove',function(cellView) {
			cellView.unhighlight(null, myHighlighter2);
			cellView.unhighlight(null, myHighlighter1);
			hideOverToolElto();
			highlighted2 = false;
});

//EXECUTE SOME DIFFERENTS ACTIONS ACCORDING OPERATION STATUS
paper.on('cell:pointerup',function(cellView,evt) {
	var entities_types = ["orm.Entity", "orm.EntityRefMode", "orm.Value"];
	var roles_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3"];
	var ISA_types = ["orm.ISA"];

	var type_cellView=cellView.model.attributes.type

	if(waiting_ISA_element && entities_types.includes(type_cellView))
	{
		 //VERIFY NOT SUBTYPING THE SAME ELEMENT
		 if(tools.attr('elementid')!=cellView.model.cid)
			createISA(cellView);
	}

	if(waiting_Role_constraint  && roles_types.includes(type_cellView))
	{
		var port = cellView.findAttribute('port', evt.target);
		if (port) {
			var posArray=selected_roles.indexOf(cellView.model.cid+'@'+port);
			if(posArray==-1)
			{
				cellView.model.prop('ports/groups/'+port+'/z', 10);
				selected_roles.push(cellView.model.cid+'@'+port);
			}
			else
			{
				cellView.model.prop('ports/groups/'+port+'/z', -1);
				selected_roles.splice(posArray, 1);
			}
		}
	}
	else if(waiting_ISA_constraint   && ISA_types.includes(type_cellView))
	{
		var posArray=selected_roles.indexOf(cellView.model.id+'@'+cellView.model.attributes.portCid);
		//SELECT and UNSELECT ISA
		if(posArray==-1)
		{
			//COMENTAR EN CASO DE LIBERAR ISA CONSTRAINTS CON DIFERENTES PARENTS
			if(verificarSameParent(selected_roles,cellView))
			{
				cellView.model.attr('line/targetMarker/stroke',onSelectionColor);
				cellView.model.attr('line/targetMarker/fill',onSelectionColor);
				cellView.model.attr('line/stroke',onSelectionColor);

				//cellView.highlight(null, myHighlighter2);
				selected_roles.push(cellView.model.id+'@'+cellView.model.attributes.portCid);
			}
		}
		else
		{
			cellView.model.attr('line/targetMarker/stroke','#A000A0');
			cellView.model.attr('line/targetMarker/fill','#A000A0');
			cellView.model.attr('line/stroke','#A000A0');

			cellView.unhighlight(null, myHighlighter2);
			selected_roles.splice(posArray, 1);
		}
	}
	else
	{
		waiting_ISA_element=false;
		selectElement(cellView);
		cleanGuide();
	}

	if(!$('#paletteOptions').hasClass("invisible"))
		showAtributes(cellView);
	evt.stopPropagation();

});

//SHOW THE LINK TOOLS. FOR EXAMPLE DELETE ICON BUTTON.
paper.on('link:mouseenter', function(linkView) {
    linkView.showTools();
});

//HIDES THE LINK TOOLS. FOR EXAMPLE DELETE ICON BUTTON.
paper.on('link:mouseleave', function(linkView) {
    linkView.hideTools();
});

//'element:mouseleave' AND 'element:mouseenter' ARE USED FTO REMARK SIDES OF ROLES WHERE CAN BE CONNECTED THE ROLE CONTRAINTS
paper.on('element:mouseenter', function(cellView, evt) {
	if(waiting_Role_constraint)
	{
		//alert(waiting_Role_constraint);
		var type_cell=cellView.model.prop('type');
		switch (type_cell)
		{
			case 'orm.Role_1':
					//anda esto tmb->cellView.model.prop('ports/groups/left/attrs/portBody/fill', 'lightblue');
					cellView.model.prop('ports/groups/left/z', 10);

			break;

			case 'orm.Role_2':
					//anda esto tmb->cellView.model.prop('ports/groups/left/attrs/portBody/fill', 'lightblue');
					cellView.model.prop('ports/groups/left/z', 10);
					cellView.model.prop('ports/groups/right/z',11);
					cellView.model.prop('ports/groups/center/z', 12);
					//cellView.model.prop('ports/groups/center/attrs/portBody/width', 20);
					//cellView.model.prop('ports/groups/center/attrs/portBody/x', 25);
					//alert(graph.getCell('c42').attributes.ports.groups.left.attrs.pp.fill);

			break;
		}
		/*DETECTA EL PUERTO SEGUN EL EVENTO QUE DEFINAMOS ARRIBA
		var port = cellView.findAttribute('port', evt.target);
		if (port) {
			console.log(port)
		}
		*/
  }
});

//'element:mouseleave' AND 'element:mouseenter' ARE USED FTO REMARK SIDES OF ROLES WHERE CAN BE CONNECTED THE ROLE CONTRAINTS
paper.on('element:mouseleave', function(cellView, evt) {
	/*cellView.unhighlight(null, myHighlighter2);
	cellView.unhighlight(null, myHighlighter1);
	hideOverToolElto();*/

	if(waiting_Role_constraint)
	{
		var posArray;
		var type_cell=cellView.model.prop('type');
		switch (type_cell)
		{
			case 'orm.Role_1':
					//cellView.model.prop('ports/groups/left/attrs/portBody/fill', 'lightblue');
					posArray=selected_roles.indexOf(cellView.model.cid+'@left');
					if(posArray==-1)
						cellView.model.prop('ports/groups/left/z', -1);

			break;
			case 'orm.Role_2':
					//cellView.model.prop('ports/groups/left/attrs/portBody/fill', 'lightblue');
					posArray=selected_roles.indexOf(cellView.model.cid+'@left');
					if(posArray==-1)
						cellView.model.prop('ports/groups/left/z', -1);

					posArray=selected_roles.indexOf(cellView.model.cid+'@right');
					if(posArray==-1)
						cellView.model.prop('ports/groups/right/z', -1);

					posArray=selected_roles.indexOf(cellView.model.cid+'@center');
					if(posArray==-1)
						cellView.model.prop('ports/groups/center/z', -1);

					//cellView.model.prop('ports/groups/center/attrs/portBody/width', 4);
					//cellView.model.prop('ports/groups/center/attrs/portBody/x', 33);
					//alert(graph.getCell('c42').attributes.ports.groups.left.attrs.pp.fill);
			break;
		}
  }
  evt.stopPropagation();
});

function verificarSameParent(selected_roles,cellView)
{
	if(selected_roles.length==0)
		return true;
	else
	{

		var id=((selected_roles[0]).split('@'))[0];
		var target_id_firstElement=graph.getCell(id).attributes.target.id;
		var target_id_newElement=cellView.model.attributes.target.id;
		return (target_id_firstElement==target_id_newElement);
	}

}



function cloneElement(type_element){
	var newElement;
	//alert(type_element);
	switch(type_element) {
		case "orm.Value":
			newElement= new orm.Value();
	newElement.attributes.uri=prefix_Url_Ontology+"Value";
			break;
		case "orm.EntityRefMode":
			newElement= new orm.EntityRefMode();
	newElement.attributes.uri=prefix_Url_Ontology+"Entity";
			break;
		case "orm.Entity":
			newElement= new orm.Entity();
	newElement.attributes.uri=prefix_Url_Ontology+"Entity";
			break;
		case "orm.Role_1":
			newElement= new orm.Role_1({
			attrs:{'role_name': {text: 'role_name'}},
			ports: { items: [{ id:'left',group: 'left' }]}});
			break;
		case "orm.Role_2":
			//return new orm.Role_2();
			newElement= new orm.Role_2({
			attrs:{'role_name': {text: 'role_name'}},
			ports: { items: [{ id:'left',group: 'left' },{id:'center', group: 'center'},{id:'right', group: 'right'}]}});
			break;
		case "orm.Union":
			newElement= new orm.Union({});
			break;
		case "orm.ExclusiveExhaustive":
			newElement= new orm.ExclusiveExhaustive({});
			break;
		case "orm.Exclusive":
			newElement= new orm.Exclusive({});
			break;
		case "orm.Equality":
			newElement= new orm.Equality({});
			break;
		case "orm.Subset":
			newElement= new orm.Subset({});
			break;
		default:
			//alert('Nothing to clone');
			break;
	};
	return newElement;
}

//SETS COLORS ON CONFIG PAPPER OPTIONS
function pupulateSelectColor(){
	var index=0;
	//htmlHexColors.sort();
	htmlHexColors.forEach(function(color){
		//alert(color);
		var opt = document.createElement("option");
		var opt_bis;
		opt.value= color;
		opt.setAttribute("style", "background:" +color+";");

		opt.innerHTML = color; // whatever property it has

		// then append it to the select element
		$('#paperBgColor').append(opt);

		opt_bis=$(opt).clone();
		//alert(opt_bis);
		$('#gridBgColor').append(opt_bis);

		index++;
	});
}

function cancel(){
	elementos_seleccionados=[];
	estado_actual=0;
	esprando_seleccion=false;
	cleanGuide();
	$('#lbl_elementos_necesarios').html('<strong>0</strong>');
	$('#lbl_elementos_seleccionados').html('<strong>0</strong>');
}

//CURRENT STATE (ESTADO ACTUAL) INDICATES CURRENT OPERATION
paper.on('cell:pointerup', function(cellView, evt, x, y) {
	$('#cid').text(cellView.model.cid );
	//CONTROLO ESTADO
	switch(estado_actual) {
		//UNARY ROLE
		case 1:
			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');
				showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');
			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}
			break;
		//BINARY ROLE
		case 2:

			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');
			showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');

			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}
			break;
		//ADD DOSJOINT
		case 3:
			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');

			showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');

			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}
			break;
		//ADD ISA
		case 4:
			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');

			showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');

			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}

			break;
		//TERNARY ROLE
		case 5:

			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');

			showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');

			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}
			break;
		//ADD DISJOINT
		case 6:
			elementos_seleccionados.push(cellView.model.cid);
			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');

			showGuide('Selected Elements:<strong>'+cantidad+'/'+cantidad_elementos_seleccionar+'</strong>');
			if(cantidad_elementos_seleccionar==cantidad)
			{
				createRole(elementos_seleccionados);
			}
			break;
		case 9:

			var cantidad=elementos_seleccionados.length;
			$('#lbl_elementos_seleccionados').html('<strong>'+cantidad+'</strong>');
			graph.removeCells(graph.getCell(cellView.model.cid));
			break;
	}
});

//FUNCTION THAT HIDES ALL THE POSSIBLE ACTION BUTTONS ON THE SELECTED ELEMENTS
function hideOverToolElto(){
  $(".ot_elto" ).each(function() {
	$(this).css({display: 'none'});
	});
}

//HIDE WHOLE TOOL
function hideElementTools() {
	tools.css('display', 'none');
	actualElement = null;
}

//FUNCTION THAT SHOWS SOME OF THE BUTTONS ACCORDING TO THE SELECTED ITEM
function showElementToolsElement(cellView) {
	var beggining_pos_y=$('#r_w_fixTool').height()+	35;

	var roles_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3","erd.CustomEntity"];
	var constraint_types = ["orm.Union", "orm.Disj1", "orm.ExclusiveExhaustive", "orm.Exclusive", "orm.Equality", "orm.Subset"];
	var entity_types = ["orm.Entity", "orm.EntityRefMode", "orm.Value"];

	var show_principals=false;
	var type_cellView=cellView.model.attributes.type;
	var pos = paper.localToClientPoint(cellView.model.attributes.position);

    tools.width($("#"+cellView.id).width());
    tools.height($("#"+cellView.id).height());
    tools.attr('elementid', cellView.model.cid);

    tools.css({
        top: pos.y-beggining_pos_y,
        left: pos.x-103,
        display: 'block'
    });

	if(entity_types.includes(type_cellView))
	{
		$('#elementISA').css({
			display: "block"
		});

		 $('#elementUnLinkButton').css({
			display: "block"
		});
		show_principals=true;
	}
	else if(roles_types.includes(type_cellView))
	{
		$('#elementUnLinkButton').css({
			display: "block"
		});

		$('#elementLinkButton').css({
			display: "block"
		});
		show_principals=true;
	}
	else if(constraint_types.includes(type_cellView))
	{
		$('#elementUnLinkButton').css({
			display: "block"
		});

		$('#elementRoleConstraint').css({
			display: "block"
		});

		$('#elementISAConstraint').css({
			display: "block"
		});
		show_principals=true;
	}
	if(show_principals)
	{
		$('#elementDeleteButton').css({
			display: "block"
		});

		$('#elementDuplicateButton').css({
			display: "block"
		});
	}
}

//ALLOW ZOOM IN AND ZOOM OUT USING MOUSE WHEEL
$('#r_w').on('mousewheel DOMMouseScroll', function (evt, x, y) {
	unSelectAll();
	$('.zoomBtn.active').removeClass('active');
	evt.preventDefault();
	var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
	var newScale = paper.scale().sx + delta / 50;
    if (newScale > 0.4 && newScale < 2) {
		paper.scale(newScale, newScale); //, p.x, p.y);
	}
});

//HIGHLIGHT ELEMENT ON SELECTED ITEM
function selectElement(cellView){
	//CONTROL MULTISELECTION ELEMENTS
	if(!ctrl_down)
	{
		unSelectAll();
	}
	cellView.highlight(null, myHighlighter2);
	cellView.highlight(null, myHighlighter1);

	cellView.model.toFront();
	showElementToolsElement(cellView);

	//REMARK THOSE CONNECTIONS WHEN TYPE IS UNARY, BINARY O TERNARY
	var roles_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3"];
	var type_cellView=cellView.model.attributes.type

	if(roles_types.includes(type_cellView))
	{
		var cell = cellView.model || cellView;
		var inboundLinksCount = graph.getConnectedLinks(cell);
		//alert(inboundLinksCount[0]);
		//VER QUE MOSTRAR Y RESALTAR CONSIDERANDO QUE EN OTRAS FUNCIONES SE VERIFICAN
		// LOS ESTILOS EN MULTIPLES SELECCIONES
	}
}

//FUNCTION THA UNLINK ALL OUTBOUNDS LINKS. JUST OUTBOUNDS
function elementUnLink(){
	var cellSelected=graph.getCell(tools.attr('elementid'));
	var outboundLinks = graph.getConnectedLinks(cellSelected);//myElement, { outbound: true })
	graph.removeCells(outboundLinks);
}

//FUNCTION THAT ALLOWS CLONING ANY KIND OF ELEMENT. CREATE A DRAG AND DROPP STYLE COPY
function elementDuplicate(){
	cloning_element=true;
	var cellSelected=graph.getCell(tools.attr('elementid'));

	var cellViewSelected=cellSelected.findView(paper);
	var new_object =cellSelected.clone();
	var posElto=(cellSelected.position()).toString();
	posElto=posElto.split("@");

	x=posElto[0];
	y=posElto[1];

	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);

	hideElementTools();
	$('body').append('<div id="flyPaper" style="position:fixed;z-index:100;opacity:.7;pointer-event:none;"></div>');
	var flyGraph = new joint.dia.Graph,
	flyPaper = new joint.dia.Paper({
		  el: $('#flyPaper'),
		  model: flyGraph,
		  interactive: false
	});
	flyShape = cellSelected.clone();
	pos = cellSelected.position();
	offset = {
		  x: x - pos.x +1,
		  y: y - pos.y +1
	};

	flyShape.position(0, 0);
	flyGraph.addCell(flyShape);
	//flyShape.position(1, 1);
	$("#flyPaper").offset({
		left: $(this).pageX - offset.x,
		top: (this).pageY - offset.y
	});
	$('body').on('mousemove.fly', function(e) {
	$("#flyPaper").offset({
		left: e.pageX - offset.x,
		top: e.pageY - offset.y
	});
	});
	$('body').on('mouseup.fly', function(e) {
		var x = e.pageX,
		  y = e.pageY,
		  target = paper.$el.offset();

		// Dropped over paper ?
		if (x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) {
		  var s = flyShape.clone();
		  s.position(x - target.left - offset.x, y - target.top - offset.y);
		  graph.addCell(s);
		  var cellView=s.findView(paper);
			//selectOne(cellView);
			selectElement(cellView);
			showAtributes(cellView);
		}
		$('body').off('mousemove.fly').off('mouseup.fly');
		flyShape.remove();
		$('#flyPaper').remove();

		cloning_element=false;
		linking_roles=false;
		estado_actual=0;
	});
}

//FUNCTION THAT PREPARS INTERAFCE FOR SELECT ROLES TO CREATE THE ROLE CONTRAINT FORM
//WILL BE DRAWED AFTER PRESSING ENTER KEY
function roleConstraint(event){
	showGuide('Select all ROLES SIDES and press <strong>ENTER</strong> when finished or press <strong>ESCAPE</strong> to cancel...');
	event.stopPropagation();
	//alert('roleConstraint');
	selected_roles=[];
	tools.attr('elementIdWaitintg',tools.attr('elementid'));
	waiting_Role_constraint=true;
	waiting_ISA_constraint=false;

	hideElementTools();

	var cellSelected=graph.getCell(tools.attr('elementid'));
	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);

	cellSelected.attr('external_ellipse/stroke',onSelectionColor);
	cellSelected.attr('internal_ellipse/stroke',onSelectionColor);
	cellSelected.attr('internal_ellipse/fill',onSelectionColor);

	cellSelected.attr('line1/stroke',onSelectionColor);
	cellSelected.attr('line2/stroke',onSelectionColor);
	cellSelected.attr('simb/stroke',onSelectionColor);
	cellSelected.attr('simb/fill',onSelectionColor);
}

//THE SAME AS (roleConstraint) BUT FOR ISA CONSTRAINT.
function isaConstraint(event){
	showGuide('Select all ISA (<strong>with same parent</strong>) and press <strong>ENTER</strong> when finished or press <strong>ESCAPE</strong> to cancel...');

	event.stopPropagation();
	//alert('roleConstraint');
	selected_roles=[];
	tools.attr('elementIdWaitintg',tools.attr('elementid'));
	waiting_ISA_constraint=true;
	waiting_Role_constraint=false;
	hideElementTools();

	var cellSelected=graph.getCell(tools.attr('elementid'));
	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);

	cellSelected.attr('external_ellipse/stroke',onSelectionColor);
	cellSelected.attr('internal_ellipse/stroke',onSelectionColor);
	cellSelected.attr('internal_ellipse/fill',onSelectionColor);

	cellSelected.attr('line1/stroke',onSelectionColor);
	cellSelected.attr('line2/stroke',onSelectionColor);
	cellSelected.attr('simb/stroke',onSelectionColor);
	cellSelected.attr('simb/fill',onSelectionColor);

}

function createIsaConstraint(){
	waiting_ISA_constraint=false;

	var cellSelected=graph.getCell(tools.attr('elementIdWaitintg'));
	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);
	var band=true;
	var Cid_PortName=[];
	selected_roles.forEach(function(element) {

		cid_PortName=element.split('@');

		elementIsa=graph.getCell(cid_PortName[0]);
		elementIsa.attr('line/targetMarker/stroke','#A000A0');
		elementIsa.attr('line/targetMarker/fill','#A000A0');
		elementIsa.attr('line/stroke','#A000A0');

		if((cellSelected.prop('type')=='orm.Subset') && band)
		{
			var new_link = new joint.shapes.orm.DashedLinkArrow({
				target: {
					id: cid_PortName[0],
					port: cid_PortName[1]
				}
			});
			band=false;
		}
		else
		{
			var new_link = new joint.shapes.orm.DashedLink({
				target: {
					id: cid_PortName[0],
					port: cid_PortName[1]
				}
			});
		}
		new_link.source(cellSelected);
		graph.addCell(new_link);
		add_hide_Tools(new_link);
		new_link.toBack();

	});
	cellSelected.attr('internal_ellipse/stroke','#A000A0');
	cellSelected.attr('external_ellipse/stroke','#A000A0');
	cellSelected.attr('internal_ellipse/fill','#A000A0');

	cellSelected.attr('line1/stroke','#A000A0');
	cellSelected.attr('line2/stroke','#A000A0');
	cellSelected.attr('simb/stroke','#A000A0');
	cellSelected.attr('simb/fill','#A000A0');

	cleanGuide();
	unSelectAll();
}

//FUNCTION THAT SETS RANDON POSITION TO ELEMENT PASSED AS ATTRIBUTE,
//ACCORDING TO PAPPER SIZE AND ELEMENTE SIZE
function setRndPosition(element) {

	var min_x=element.attributes.size.width;
	var max_x=paper.options.width-element.attributes.size.width;
	var x=Math.floor(Math.random() * (max_x - min_x + 1) ) + min_x;

	var min_y=element.attributes.size.height;
	var max_y=paper.options.height-element.attributes.size.height;
	var y=Math.floor(Math.random() * (max_y - min_y + 1) ) + min_y;

	element.position(x,y);
}

function showGuide(msg)
{
	$('#guideFeedback').html(msg);
	//$('#paletteOptions').css('display', 'none');
	$('#paletteOptions').addClass('invisible');
	$('#palettePaperOptions').addClass('invisible');
	$('#paletteMsg').removeClass('invisible');
	//$('#paletteMsg').show('slow');
}

function cleanGuide()
{
	$('#guideFeedback').html('');
	$('#paletteMsg').addClass('invisible');
	//$('#paletteMsg').hide('fast');
}

//UNHIGHLIGHT ALL ELEMENTS OF DE GRAPH
function unSelectAll(){
	tools.attr('elementid',-1)
	var cv;
	var cellsG=graph.getCells();
	cellsG.forEach(function(c){
		c.findView(paper).unhighlight(null, myHighlighter2);
		c.findView(paper).unhighlight(null, myHighlighter3);
		c.findView(paper).unhighlight(null, myHighlighter1);
		hideOverToolElto();
		highlighted2 = false;
	})
}

//UNHIGHLIGHT ELEMENT
function unSelect(elementModel){
	tools.attr('elementid',-1)
	var ev=elementModel.findView(paper);
	ev.unhighlight(null, myHighlighter2);
	//elementView.unhighlight(null, myHighlighter3);
	ev.unhighlight(null, myHighlighter1);
	hideOverToolElto();
	highlighted2 = false;
}

//HIDE THE ATTRIBUTES OF THE ATTRIBUTE PALETTE SITUATED TO THE RIGHT OF THE TOOL
//GENERALLY USED BEFORE A DBLCLICL CELL O ELEMENTE SELECTION
function hideAtributes(){

	$('#paletteOptions').addClass("invisible");
	$( "#paletteOptions div .eT,.eR, .eV, .eROLE, .eROLE_2, .customLink" ).each(function() {
		$(this).addClass("invisible");
	  });
}

//SHOW THE ATTRIBUTES ACCORDING TO DE ELEMENT SELECTED BY DBLCLICK EVENT.
function showAtributes(cellView){
	$('#palettePaperOptions').addClass('invisible');
	hideAtributes();
	var type_selected=cellView.model.prop('type');
	$('#typeSelected').text(type_selected);
	$('#paletteOptions').removeClass("invisible");
	//alert(type_selected);
	switch (type_selected)
	{
		case 'orm.Entity':
			 $( "div .eT" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  $('#entityName').val(cellView.model.attributes.attrs.label.text);
        $('#URIDefault').val(cellView.model.attributes.uri);

		break;
		case 'orm.Value':
			 $( "div .eV" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  $('#entityName').val(cellView.model.attributes.attrs.label.text);
        $('#URIDefault').val(cellView.model.attributes.uri);

		break;
		case 'orm.EntityRefMode':
			$( "div .eR" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  $('#entityName').val(cellView.model.attributes.attrs.label.text);
			  var refMode_Name=cellView.model.attributes.attrs.refMode.text.replace('(','');
			  refMode_Name=refMode_Name.replace(')','');
			  $('#entityReference').val(refMode_Name);
        $('#URIDefault').val(cellView.model.attributes.uri);
		break;
		case 'orm.Role_1':
			$( "div .eROLE" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  $('#roleName').val(cellView.model.attributes.attrs.role_name.text);
			  $('#ddl_InverseReading option[value="' + cellView.model.attributes.inverseReading +'"]').prop("selected", true);
			  $('#ddl_OverRoleName option[value="' + cellView.model.attributes.overHeadRoleName +'"]').prop("selected", true);

		break;
		case 'orm.Role_2':
			$( "div .eROLE_2" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  //alert(cellView.model.attributes.inverseReading);
			  //alert(cellView.model.attributes.overHeadRoleName);
			  $('#roleName').val(cellView.model.attributes.attrs.role_name.text);
			  $('#ddl_Cardinality option[value="' + cellView.model.attributes.cardinality +'"]').prop("selected", true);
			  $('#ddl_InverseReading option[value="' + cellView.model.attributes.inverseReading +'"]').prop("selected", true);
			  $('#ddl_OverRoleName option[value="' + cellView.model.attributes.overHeadRoleName +'"]').prop("selected", true);

		break;
		case 'orm.CustomLink':
			$( "div .customLink" ).each(function() {
				$(this).removeClass("invisible");
			  });
			  var visible= cellView.model.attr('line/targetMarker/display');
			  $('#ddl_Mandatory option[value="' + visible +'"]').prop("selected", true);
		break;
	}
}

//DELETE ELEMENT SELECTED USING THE CID VALUE STORED IN TOOLS.ATTR(ELEMENTID)
function elementDelete() {
	var cell = graph.getCell(tools.attr('elementid'));
	cell.remove();
	//actualElement.remove();
	hideElementTools();
}

//CONFIRM CHANGES OF THE PAPPER FROM PALLET CONFIGURATION
function applyPaperChanges(){
	paper.drawBackground({color:$('#paperBgColor').val()});
	if($('#ddl_showGrid').val()==0)
	{
		paper.setGrid('drawGrid:false');

	}
	else
	{
		paper.setGrid('drawGrid:true');
		paper.options.gridSize = $('#gridSize').val();
		paper.drawGrid();
		paper.setGrid({ name: 'dot', args: { color: $('#gridBgColor').val(),thickness: $('#gridThickness').val() }});
		paper.drawGrid();
	}
}

//CONFIRM CHANGES OF THE SELECTED ELEMENT ACCORDING ITS TYPE
function applyChanges(){

	var cellSelected=graph.getCell(tools.attr('elementid'));
	unSelect(cellSelected);
	switch ($('#typeSelected').text())
	{
		case 'orm.Entity':
			cellSelected.attr('label',{text:$.trim($('#entityName').val())});
			cellSelected.attributes.uri=prefix_Url_Ontology+ $.trim($('#entityName').val());
			resizeElementFromLabel(getView(cellSelected));
			break;
		case 'orm.Value':
			cellSelected.attr('label',{text:$.trim($('#entityName').val())});
			cellSelected.attributes.uri=prefix_Url_Ontology+ $.trim($('#entityName').val());
			resizeElementFromLabel(getView(cellSelected));
			break;
		case 'orm.EntityRefMode':
			cellSelected.attr('label',{text:$.trim($('#entityName').val())});
			cellSelected.attr('refMode',{text: '('+$.trim($('#entityReference').val())+')'});
			cellSelected.attributes.uri=prefix_Url_Ontology+ $.trim($('#entityName').val());
			resizeElementFromLabel(getView(cellSelected));
			break;
		case 'orm.Role_1':
			cellSelected.attr('role_name',{text:$.trim($('#roleName').val())});
			var new_reading=$('#ddl_InverseReading').val();
			var new_overHeadRoleName=$('#ddl_OverRoleName').val();

			cellSelected.attr(new_cardinality+'/display', 'inline');
			cellSelected.attributes.cardinality=new_cardinality;

			(new_reading==1) ? cellSelected.attr('left_read/display', 'inline') : cellSelected.attr('left_read/display', 'none');
			cellSelected.attributes.inverseReading=new_reading;

			(new_overHeadRoleName==1) ? cellSelected.prop('attrs/role_name/y',-20): cellSelected.prop('attrs/role_name/y',35);
			cellSelected.attributes.overHeadRoleName=new_overHeadRoleName;
			break;
		case 'orm.Role_2':
			cellSelected.attr('role_name',{text:$.trim($('#roleName').val())});
			cellSelected.attr(cellSelected.attributes.cardinality+'/display', 'none');
			var new_cardinality=$('#ddl_Cardinality').val();
			var new_reading=$('#ddl_InverseReading').val();
			var new_overHeadRoleName=$('#ddl_OverRoleName').val();

			cellSelected.attr(new_cardinality+'/display', 'inline');
			cellSelected.attributes.cardinality=new_cardinality;

			(new_reading==1) ? cellSelected.attr('left_read/display', 'inline') : cellSelected.attr('left_read/display', 'none');
			cellSelected.attributes.inverseReading=new_reading;

			(new_overHeadRoleName==1) ? cellSelected.prop('attrs/role_name/y',-20): cellSelected.prop('attrs/role_name/y',35);
			cellSelected.attributes.overHeadRoleName=new_overHeadRoleName;

			apply_InternalFrequencyConstraint(cellSelected,'Left');
			apply_InternalFrequencyConstraint(cellSelected,'Right');
			break;

		case 'orm.CustomLink':
			cellSelected.attr('line/targetMarker/display',$('#ddl_Mandatory').val());
			break;
	}
	//getView(cellSelected).highlight(null, myHighlighter2);
	hideAtributes();
}

//HIGHLIGHT POSIBLE ELEMENTS THAT CAN BE CONNECTED TO CREATE ISA
function showIsaPosibleConnections(){
	var cellsG=graph.getCells();
	var cv;
	cellsG.forEach(function(c){
		cv=c.findView(paper);
		//alert((tools.attr('elementid')==(cv.model.cid)));
		if((tools.attr('elementid'))!=(cv.model.cid))
		{
			switch (c.prop('type'))
			{
				case 'orm.EntityRefMode':
					cv.highlight(null, myHighlighter3);
					break;
				case 'orm.Entity':
					cv.highlight(null, myHighlighter3);
					break;
				}
		}
	});
	showGuide('Select one of the possible Entities for Subtyping or press <strong>ESCAPE</strong> to cancel...');

	waiting_ISA_element=true;
}

//CREATE AN ISA CONNECTION
function createISA(cellView){
	var cellSelected=graph.getCell(tools.attr('elementid'));

	cellView.unhighlight(null, myHighlighter2);
	cellView.unhighlight(null, myHighlighter1);

	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);

	var new_object = new joint.shapes.orm.ISA();

	new_object.source(cellSelected);
	new_object.target(cellView.model);
	graph.addCell(new_object);
	add_hide_Tools(new_object);


	waiting_ISA_element=false;
	selectElement(cellView);
	cleanGuide();
	unSelectAll();
}

//CREATE ISA CONSTRAINT BETWEEN 'elementIdWaitintg' AND selected_roles
function createRoleConstraint(){

	waiting_Role_constraint=false;
	var cellSelected=graph.getCell(tools.attr('elementIdWaitintg'));
	cellSelected.findView(paper).unhighlight(null, myHighlighter2);
	cellSelected.findView(paper).unhighlight(null, myHighlighter1);
	var band=true;

	var Cid_PortName=[];
	selected_roles.forEach(function(element) {

		cid_PortName=element.split('@');
		if((cellSelected.prop('type')=='orm.Subset') && band)
		{
			var new_link = new joint.shapes.orm.DashedLinkArrow({
				target: {
					id: graph.getCell(cid_PortName[0]).id,
					port: cid_PortName[1]
				}
			});
			band=false;
		}
		else
		{
			var new_link = new joint.shapes.orm.DashedLink({
				target: {
					id: graph.getCell(cid_PortName[0]).id,
					port: cid_PortName[1]
				}
			});
		}
		new_link.source(cellSelected);
		graph.addCell(new_link);
		add_hide_Tools(new_link);
		new_link.toBack();

	});

	cellSelected.attr('internal_ellipse/stroke','#A000A0');
	cellSelected.attr('external_ellipse/stroke','#A000A0');
	cellSelected.attr('internal_ellipse/fill','#A000A0');

	cellSelected.attr('line1/stroke','#A000A0');
	cellSelected.attr('line2/stroke','#A000A0');
	cellSelected.attr('simb/stroke','#A000A0');
	cellSelected.attr('simb/fill','#A000A0');

	unSelectAll();
	unSelectRoleElements();
}

//FUNCTION THA RESIZES WIDTH ACCORDING LARGE OF TEXT INSIDE (ENTITIES TYPES)
function resizeElementFromLabel(elementView){
	elementView.unhighlight(null, myHighlighter2);
	var element=elementView.model;
	var sizeChars=0;
	var markups=element.markup;

	for (i = 0; i < markups.length; ++i) {

		if(markups[i].tagName.toUpperCase()=='TEXT')
		{
			var selector=markups[i].selector;
			$('#string_span').text(element.attr(selector+'/text'));
			if($('#string_span').width()>sizeChars)
				sizeChars=$('#string_span').width();
		}
	}
	//console.log(original_width_entities+' @ '+sizeChars);
	if(sizeChars>original_width_entities)
	{
		element.prop('size/width', sizeChars+20);
		//console.log(sizeChars+20);
		}
	else
	{
		element.prop('size/width', original_width_entities);
		//console.log(original_width_entities);
	}
}

//SHOW LOADING BANNER
function mostrarLoading(){
	$(".loader").show();
}

//CLOSE LOADING BANNER
function closeLoading(){
	$(".loader").fadeOut("fast");
}

//SCALE PAPPER FROM TOOL BUTTON. 50% 75% ETC.
function scalePapper(btn,valuePercent){
	$('.zoomBtn.active').removeClass('active');
	var originalScale=1;
	var newScale=originalScale*(valuePercent/100);
	paper.scale(newScale, newScale);
}

//FUNCTION THAT UNSELECT ALL ELEMENTS WHEN THE DISJOINT CONTRAINT IS RENDER AFTER press <strong>ENTER</strong> KEY
function unSelectDisjElements(){
	var disj_types = ["orm.Union", "orm.ExclusiveExhaustive", "orm.Exclusive", "orm.Equality", "orm.Subset"];
	var cellsG=graph.getCells();
	cellsG.forEach(function(c){
		var type_cellView=c.attributes.type

		//alert(type_cellView);
		if(disj_types.includes(type_cellView))
		{
			c.attr('external_ellipse/stroke',original_strokeFill_constraint);
			c.attr('internal_ellipse/stroke',original_strokeFill_constraint);
			c.attr('internal_ellipse/fill',original_strokeFill_constraint);

			c.attr('line1/stroke',original_strokeFill_constraint);
			c.attr('line2/stroke',original_strokeFill_constraint);
			c.attr('simb/stroke',original_strokeFill_constraint);
			c.attr('simb/fill',original_strokeFill_constraint);
		}
		else if(type_cellView=='orm.ISA')
		{
			c.attr('line/targetMarker/stroke','#A000A0');
			c.attr('line/targetMarker/fill','#A000A0');
			c.attr('line/stroke','#A000A0');
			c.findView(paper).unhighlight(null, myHighlighter2);
		}
	});
}

//FUNCTION THAT UNSELECT ALL ELEMENTS WHEN THE ROLE CONTRAINT IS RENDER AFTER press <strong>ENTER</strong> KEY
function unSelectRoleElements(){

	var disj_types = ["orm.Union", "orm.ExclusiveExhaustive", "orm.Exclusive", "orm.Equality", "orm.Subset"];
	var role_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3"];
	var cellsG=graph.getCells();
	cellsG.forEach(function(c){
		var type_cellView=c.attributes.type

		//alert(type_cellView);
		if(disj_types.includes(type_cellView))
		{
			c.attr('external_ellipse/stroke',original_strokeFill_constraint);
			c.attr('internal_ellipse/stroke',original_strokeFill_constraint);
			c.attr('internal_ellipse/fill',original_strokeFill_constraint);

			c.attr('line1/stroke',original_strokeFill_constraint);
			c.attr('line2/stroke',original_strokeFill_constraint);
			c.attr('simb/stroke',original_strokeFill_constraint);
			c.attr('simb/fill',original_strokeFill_constraint);
		}
		if(role_types.includes(type_cellView))
		{

			c.prop('ports/groups/left/z', -1);
			c.prop('ports/groups/right/z',-1);
			c.prop('ports/groups/center/z', -1);
		}
	});
}

//SET INTERNAL FREQUENCY CONTRAINT
function apply_InternalFrequencyConstraint(cell, side){
	//alert(cell);
	switch(side)
	{
		case 'Left':
			//alert('L');
			var ddl_id='ddl_Ifc_Left';
		break;
		case 'Right':
			var ddl_id='ddl_Ifc_Right';
		break;
	}


	switch ($('#'+ddl_id).children("option:selected").val())
	{
		case 'interval'://alert('interval');
			//alert('interval');
			cell.attr('ifc_'+side+'_const/display', 'inline');
			cell.attr('line_'+side+'_const/display', 'inline');
			cell.attr('ifc_'+side+'_const/text', $('#ifc'+side+'_F').val() + '...'+$('#ifc'+side+'_T').val());
			break;

		case 'none':

			cell.attr('ifc_'+side+'_const/display', 'none');
			cell.attr('line_'+side+'_const/display', 'none');
			cell.attr('ifc_'+side+'_const/text', '');

			break;
		default:
			cell.attr('ifc_'+side+'_const/display', 'inline');
			cell.attr('line_'+side+'_const/display', 'inline');
			cell.attr('ifc_'+side+'_const/text', $('#ifc'+side+'_F').val() + ' '+$('#ifc'+side+'_T').val());

			break;

	}



}

//PREPARE SLEECTIONS OF THE ELEMENT FOR THE PALLETE AFTER DBLCLICK ACTION
function verifiy_InternalFrequencyConstraint(ddl){
	var ddl_id=$(ddl).attr('id');
	switch ($(ddl).children("option:selected").val())
	{
		case 'interval'://alert('interval');
			if(ddl_id=='ddl_Ifc_Left')
			{
				$('.ifc_L' ).each(function() {
					$(this).removeAttr("disabled");
				});
				$('#ifcLeft_F').val('');
				$('#ifcLeft_T').val('');
			}
			else
			{
				$('.ifc_R' ).each(function() {
					$(this).removeAttr("disabled");
				});
				$('#ifcRight_F').val('');
				$('#ifcRight_T').val('');
			}
			break;
		case 'none':
			if(ddl_id=='ddl_Ifc_Left')
			{
				$( ".ifc_L" ).each(function() {
					$(this).attr("disabled", true);
				});
			}
			else
			{
				$( ".ifc_R" ).each(function() {
					$(this).attr("disabled", true);
				});
			}
			break;
		default:
			if(ddl_id=='ddl_Ifc_Left')
			{
				//alert('left');
				$('#ifcLeft_F').val($(ddl).children("option:selected").text())
				$('#ifcLeft_T').removeAttr("disabled");
			}
			else
			{
				$('#ifcRight_F').val($(ddl).children("option:selected").text())
				$('#ifcRight_T').removeAttr("disabled");
			}
			break;
	}
}

//CREATE JSON FROM ORM DIAGRAM AND SHOW IT IN MODAL POPUP VIEW
function exportJSON(){
	mostrarLoading();
	var json=createJSON();
	$('#modalBodyContent').html(json);
	$('#modal_Contenido').modal('show');
	closeLoading();
}

//INVOKES ENCODE SERVER SIDE FUNCTION
function encode(){
	mostrarLoading();
	var json=createJSON();
	encodingEnzoServer(json);
}

//INVOKES REASONER SERVER SIDE FUNCTION
function reasoner(){
	mostrarLoading();
	var json=createJSON();
	reasoningEnzoServer(json);
}

//ADJUST WIDTH AND HEIGHT ACCORDING RESOLUTION DETECTED WHEN DIVS AUTORESIZE
$(document).ready(function() {
	var container_w=$('#container_w').height()- $('#r_w_fixTool').height();
	$('#container_w').height(container_w+ $('#r_w_fixTool').height());
	paper.setDimensions($('#r_master').width(), container_w);
	pupulateSelectColor();
	$('#r_w').append(tools);
});

//HANDLES KEY UP CON CONTROL KEY FOR SETTING FLAG VALUE AS FALSE.
$(document).on('keyup', function ( e ) {
	if (e.key=='Control')
	{
		ctrl_down=false;
	}
});

//HANDLES EVENTS FOR CONTROL, ESCAPE AND ENTER KEYS ACCORDING SELECTIONS
$(document).on('keydown', function ( event ) {
	if (event.key=='Control')
	{
		ctrl_down=true;
	}
	if (event.key=='Escape')
	{
		/*
		alert(graph.getCell('c42').attributes.ports.groups.left.attrs.pp.fill);
		graph.getCell('c42').prop('ports/groups/left/attrs/pp/fill', 'lightblue');
		alert(graph.getCell('c42').attributes.ports.groups.left.attrs.pp.fill);
		*/
		if(waiting_ISA_constraint)
			unSelectDisjElements();
		if(waiting_Role_constraint)
			unSelectRoleElements();

		unSelectAll();
		hideAtributes();
		ctrl_down=false;
		waiting_ISA_element=false;
		waiting_Role_constraint=false;
		waiting_ISA_constraint=false;
		linking_roles=false;
		estado_actual=0;
		cleanGuide();
		event.preventDefault();


	}
	if ((event.key=='Enter')&& waiting_Role_constraint)
	{
		//alert('Enter and creating role contraint between:'+selected_roles);
		waiting_Role_constraint=false;
		createRoleConstraint();
		cleanGuide();
		event.preventDefault();
	}
	else if ((event.key=='Enter')&& waiting_ISA_constraint)
	{
		//alert('Enter and creating role contraint between:'+selected_roles);
		waiting_ISA_constraint=false;
		createIsaConstraint();
		cleanGuide();
		event.preventDefault();
	}
});

//SET POSITION TO NEWELEMENT FROM POSITION
function setPosition(newElement, position){
//NEW ELEMENT ADDED TO GRAPH.
//POSITION(POSITION.X;POSITION.Y)
newElement.position(position.x,position.y);

}

//CREATE CONSTRAINT ELEMENT PASSING GRAPH AND POSITION AS ARGUMENT
function createNewElementConstraint(type, graph, position){
	//CREATE A NEW ELEMENT CONSTRAINT. IF GRAPH NOT NULL, ADD ELEMENT TO GRAPH
	//IF POSITION NOT NULL, SET POSITION ON THE GRAPH
	var newElement;
	switch (type)
	{
		case "union":
			newElement =new orm.Union();
			break;
		case "exclusive":
			newElement =new orm.Exclusive();
			break;
		case "exclusiveExhaustive":
			newElement =new orm.ExclusiveExhaustive();
			break;

	}
	if(graph) newElement.addTo(graph);

	if(position)
		setPosition(newElement, position);
	else
		setRndPosition(newElement);

	return newElement;
}

//FUNCTION THAT CREATES TOOL VIEW WITH SOME BUTTONS. GENERALLY USED FOR LINKS ELEMENTS
function createToolView(){
	var verticesTool = new joint.linkTools.Vertices();
	var segmentsTool = new joint.linkTools.Segments();
	var sourceArrowheadTool = new joint.linkTools.SourceArrowhead();
	var targetArrowheadTool = new joint.linkTools.TargetArrowhead();
	var sourceAnchorTool = new joint.linkTools.SourceAnchor();
	var targetAnchorTool = new joint.linkTools.TargetAnchor();
	var boundaryTool = new joint.linkTools.Boundary();
	var removeButton = new joint.linkTools.Remove({
	distance: 20
	});
	return new joint.dia.ToolsView({tools: [
		/*verticesTool, segmentsTool,
		sourceArrowheadTool, targetArrowheadTool,
		sourceAnchorTool, targetAnchorTool,
		boundaryTool, */
		removeButton
	]
	});
}

/////////////////////////////////////////////////////////////////////////////////
////FUNCTIONS THAT ALIGN MULTI SELECTED ELEMENTS.////////////////////////////////
function horizontalAlign(){
	var lastCellSelected=graph.getCell(tools.attr('elementid'));

	var yPositionReference=lastCellSelected.attributes.position.y;
	//alert(yPositionReference);
	$(".highlighted").each(function() {
		paper.findView($(this)).model.prop('position/y',yPositionReference);
		//paper.findView($(this)).model.attributes.position.y=yPositionReference;
		//alert(paper.findView($(this)).model.attributes.position.y);
  });

}

function horizontalCenterAlign(){
	var lastCellSelected=graph.getCell(tools.attr('elementid'));
	var selectedHeight_middle=lastCellSelected.attributes.size.height/2;
	var yPositionReference=lastCellSelected.attributes.position.y + selectedHeight_middle;

	$(".highlighted").each(function() {
		var cellView=paper.findView($(this));
		heightObjet=cellView.model.attributes.size.height/2;
		cellView.model.prop('position/y',yPositionReference-heightObjet);
	});

}

function verticalAlign(side){
	var lastCellSelected=graph.getCell(tools.attr('elementid'));

	var xPositionReference=lastCellSelected.attributes.position.x;
	//alert(yPositionReference);
	$(".highlighted").each(function() {
		paper.findView($(this)).model.prop('position/x',xPositionReference);
		//paper.findView($(this)).model.attributes.position.y=yPositionReference;
		//alert(paper.findView($(this)).model.attributes.position.y);
  });

}

function verticalCenterAlign(side){
	var lastCellSelected=graph.getCell(tools.attr('elementid'));
	var selectedHeight_middle=lastCellSelected.attributes.size.width/2;
	var yPositionReference=lastCellSelected.attributes.position.x + selectedHeight_middle;

	$(".highlighted").each(function() {
		var cellView=paper.findView($(this));
		heightObjet=cellView.model.attributes.size.width/2;
		cellView.model.prop('position/x',yPositionReference-heightObjet);
	});
}
/////////////////////////////////////////////////////////////////////////////////

//FUNCTION THAT RETURN VIEW OF AN ELEMENT
function getView(element){
	return element.findView(paper);
}

//FUNCTION THAT RETURN THE TYPE OF AN ELEMENT
function getSpecificType(elementModel) {

	//var entities_types = ["orm.Entity", "orm.EntityRefMode", "orm.Value"];
	//var roles_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3"];
	//var ISA_types = ["orm.ISA"];

	var type = elementModel.attributes.type;
	var resultType;
	if (type === 'orm.Entity') {
		resultType = 'entity';
    } else if (type === 'orm.EntityRefMode') {
		resultType = 'entityRefMode';
    } else if (type === 'orm.Value') {
        resultType = 'value';
	} else if (type === 'orm.Role_1') {
        resultType = 'unaryFactType';
	} else if (type === 'orm.Role_2') {
        resultType = 'binaryFactType';
	} else if (type === 'orm.Role_3') {
        resultType = 'ternaryFactTyoe';
	} else if (type === 'orm.Union') {
		resultType = 'union';
	} else if (type === 'orm.ExclusiveExhaustive') {
		resultType = 'exclusiveExhaustive';
	} else if (type === 'orm.Exclusive') {
		resultType = 'exclusive';
	} else if (type === 'orm.Equality') {
		resultType = 'equality';
	} else if (type === 'orm.Subset') {
		resultType = 'subset';
	} else {
		resultType = 'error';
	}
	return resultType;
}

//RETURN MIDDLE POSITION BETWEEN TWO ELEMENTS IN ARRAY. NOT WORKING WELL.
function calculatePosition(array_eltos){



	var posiciones =[];
	var elto_a=graph.getCell(array_eltos[0]);
	var elto_b=graph.getCell(array_eltos[1]);

	var pos_a=elto_a.position();
	var pos_b=elto_b.position();

	var y_pos_new=pos_a.y;
	var x_pos_new=pos_a.x;

	var width_a=elto_a.attributes.size.width;
	var width_b=elto_b.attributes.size.width;

	var height_a=elto_a.attributes.size.height;
	var height_b=elto_b.attributes.size.height;

	if(pos_a.y>pos_b.y)
	{
		y_pos_new=((pos_a.y-pos_b.y)/2)+pos_b.y+(height_b/2);
		//alert(pos_a.y + '-' +pos_b.y + '-' +y_pos_new);
	}
	else
	{
		y_pos_new=((pos_b.y-pos_a.y)/2)+pos_a.y+(height_a/2);
		//alert(pos_a.y + '-' +pos_b.y + '-' +y_pos_new);
	}


	if(pos_a.x>pos_b.x)
	{
		x_pos_new=((pos_a.x-pos_b.x)/2)+pos_b.x+(width_b/2);

	}
	else
	{

		x_pos_new=((pos_b.x-pos_a.x)/2)+pos_a.x+(width_a/2);
	}

	posiciones.push(x_pos_new,y_pos_new);
	return posiciones;
}

//FUNCTION THAT CREATE ROLE CONNECTIONS.REVIEW DISJ IF IS NECESARY OR NO WORKING
function createRole(array_eltos){

	hideOverToolElto();
	//ESTADO ACTUAL ES UNA VARIABLE O INPUT EN DONDE SE ALMACENA UN NUMERO (ESTADO) QUE DEFINE QUE TIENE Q DIBUJAR LA HERRAMIENTA.
	//SE PUEDE MEJORAR.
	switch(estado_actual) {
	//UNARY ROLE
	case 1:

		graph.getCell(array_eltos[0]).findView(paper).unhighlight(null, myHighlighter2);
		graph.getCell(array_eltos[0]).findView(paper).unhighlight(null, myHighlighter1);

		var role=graph.getCell(tools.attr('elementid_Role'));
		var new_link_L = new joint.shapes.orm.CustomLink({
			source: {
				id: role.id,
				port: 'left'
			}
		});
		new_link_L.target(graph.getCell(array_eltos[0]));

		//graph.addCell(role);
		graph.addCell(new_link_L);
		add_hide_Tools(new_link_L);


		var elto=graph.getCell(array_eltos[0]);
		var selectedHeight_middle=elto.attributes.size.height/2;
		var yPositionReference=elto.attributes.position.y + selectedHeight_middle;
		var pos=elto.position();

		var width=elto.attributes.size.width;
		var heightRole=role.attributes.size.height/2;
		role.position((pos.x+width+80),yPositionReference-heightRole);
		linking_roles=false;
		cancel();



		break;

	//BINARY ROLE
	case 2:

		/*
		var nombre_Role = prompt("Nombre del ROLE", "");
		var role = new orm.Role_2({
		attrs:{'.role_name': {text: nombre_Role}},
		ports: { items: [{ id:'left',group: 'left' },{id:'right', group: 'right'}]}});
		*/
		var role=graph.getCell(tools.attr('elementid_Role'));
		//alert(tools.attr('elementid_Role'));

		//alert(graph.getCell(array_eltos[0]));
		graph.getCell(array_eltos[0]).findView(paper).unhighlight(null, myHighlighter2);
		graph.getCell(array_eltos[0]).findView(paper).unhighlight(null, myHighlighter1);

		graph.getCell(array_eltos[1]).findView(paper).unhighlight(null, myHighlighter2);
		graph.getCell(array_eltos[1]).findView(paper).unhighlight(null, myHighlighter1);

		var new_link_L = new joint.shapes.orm.CustomLink({
			//COMENTO SOURCE XQ NO LEVANTA LOS GET LINKS CONNECTED
			/*source: {
			id: array_eltos[0]},*/
			source: {
				id: role.id,
				port: 'left'
			}
		});
		new_link_L.target(graph.getCell(array_eltos[0]));


		var new_link_R =  new joint.shapes.orm.CustomLink({
			/*source: {
			id: array_eltos[1]},*/
			source: {
				id: role.id,
				port: 'right'
			}
		});
		new_link_R.target(graph.getCell(array_eltos[1]));


		graph.addCell(role);
		graph.addCell(new_link_R);
		graph.addCell(new_link_L);

		add_hide_Tools(new_link_R);
		add_hide_Tools(new_link_L);

		linking_roles=false;

		cancel();
		break;
	case 3:

		switch($('#ddl_disj').val()) {
			case 'D1':
				var d= new orm.Union({});
			break;

			case 'D2':
				var d= new orm.ExclusiveExhaustive({});
			break;

			case 'D3':
				var d= new orm.Exclusive({});
			break;

			case 'D4':
				var d= new orm.Equality({});
			break;

			case 'D5':
				var d= new orm.Subset({});
			break;
		}

		d.addTo(graph);
		var new_link_L = new joint.shapes.devs.Link();
		new_link_L.source(graph.getCell(array_eltos[0]));
		new_link_L.target(d);

		var new_link_R = new joint.shapes.devs.Link();
		new_link_R.source(graph.getCell(array_eltos[1]));
		new_link_R.target(d);

		new_link_R.addTo(graph);
		new_link_L.addTo(graph);

		add_hide_Tools(new_link_R);
		add_hide_Tools(new_link_L);
		/*
		var nueva_posicion=calculatePosition(array_eltos);
		d.position(nueva_posicion[0],nueva_posicion[1]);
		*/
		cancel();
		break

	case 4:
		var new_link_R = new joint.shapes.orm.ISA();
		new_link_R.source(graph.getCell(array_eltos[1]));
		new_link_R.target(graph.getCell(array_eltos[0]));


		new_link_R.addTo(graph);
		add_hide_Tools(new_link_R);
		cancel();
		break;
	//TERNARY ROLE
	case 5:

		var role = new orm.Role_3({
		ports: { items: [{ id:'left',group: 'left' },{id:'middle_id', group: 'middle'},{id:'right', group: 'right'}]}});


		var new_link_L = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[0]},
			target: {
				id: role.id,
				port: 'left'
			}
		});

		var new_link_M = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[1]},
			target: {
				id: role.id,
				port: 'middle_id'
			}
		});

		var new_link_R = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[2]},
			target: {
				id: role.id,
				port: 'right'
			}
		});

		graph.addCell(role);
		graph.addCell(new_link_R);
		graph.addCell(new_link_M);
		graph.addCell(new_link_L);

		add_hide_Tools(new_link_R);
		add_hide_Tools(new_link_M);
		add_hide_Tools(new_link_L);
		/*
		var nueva_posicion=calculatePosition(array_eltos);
		role.position(nueva_posicion[0],nueva_posicion[1]);
		*/

		cancel();
		break;
	//ISA DISJOINT
	case 6:

		switch($('#ddl_disj').val()) {


		case 'D1':
			var d= new orm.Union({});
		break;

		case 'D2':
			var d= new orm.ExclusiveExhaustive({});
		break;

		case 'D3':
			var d= new orm.Exclusive({});
		break;

		case 'D4':
			var d= new orm.Equality({});
		break;

		case 'D5':
			var d= new orm.Subset({});
		break;
		}

		d.addTo(graph);

		var new_link_L = new joint.shapes.orm.ISA_Disj();
		new_link_L.source(graph.getCell(array_eltos[1]));
		new_link_L.target(d);

		var new_link_R = new joint.shapes.orm.ISA_Disj();
		new_link_R.source(graph.getCell(array_eltos[2]));
		new_link_R.target(d);

		var new_link_P = new joint.shapes.orm.ISA();
		new_link_P.target(graph.getCell(array_eltos[0]));
		new_link_P.source(d);

		new_link_R.addTo(graph);
		new_link_L.addTo(graph);
		new_link_P.addTo(graph);

		add_hide_Tools(new_link_R);
		add_hide_Tools(new_link_L);
		add_hide_Tools(new_link_P);

		/*
		var nueva_posicion=calculatePosition(array_eltos);
		d.position(nueva_posicion[0],nueva_posicion[1]);
		*/
		cancel();
		break
	}

}

//FUNCTION THAT IS CALL FROM THE ELEMENTS, TO CREATE CONNECTIONS
function linkElementType(){
	$('#guideFeedback').html('');
	var type_selection=graph.getCell(tools.attr('elementid')).prop('type');
	switch(type_selection) {
		case 'orm.Role_1':
			$('#lbl_elementos_necesarios').html('<strong>1</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=1;
			estado_actual=1;
			tools.attr('elementid_Role', tools.attr('elementid'));
			linking_roles=true;
			showGuide('Select one Entity Type or press <strong>ESCAPE</strong> to cancel...');
			break;
		case 'orm.Role_2':
			$('#lbl_elementos_necesarios').html('<strong>2</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=2;
			estado_actual=2;
			tools.attr('elementid_Role', tools.attr('elementid'));
			linking_roles=true;
			showGuide('Select two Entities Type or press <strong>ESCAPE</strong> to cancel...');

			break;
		case 'TR':
			$('#lbl_elementos_necesarios').html('<strong>3</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=3;
			estado_actual=5;
			showGuide('Select three Entities Type or press <strong>ESCAPE</strong> to cancel...');

			break;
		case 'DIS':
			$('#lbl_elementos_necesarios').html('<strong>2</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=2;
			estado_actual=3;
			break;
		case 'ISA':
			$('#lbl_elementos_necesarios').html('<strong>2</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=2;
			estado_actual=4;
			break;
		case 'ISA_D':
			$('#lbl_elementos_necesarios').html('<strong>3</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=3;
			estado_actual=6;
			break;
		case 'REMOVE':
			$('#lbl_elementos_necesarios').html('<strong>1</strong>');
			elementos_seleccionados=[];
			cantidad_elementos_seleccionar=1;
			estado_actual=9;
			break;
	}
}

//FUNCTION THAT RETURNS AN ARRAY WITH SPECIFIC FORMAT OF ALL ELEMENTS OF THE GRAPH
//FUNCTION CALLED FROM CREATEJSON();
function getAllElement() {

	//retorna todos los elementos en un arreglo con la forma [entidades,relaciones,atributos,herencias,conectores]
	var entities = [];
    var relationships = [];
    var attributes = [];
    var inheritances = [];
    var links = [];
    var elements = graph.getElements();
	//alert(elements);
    for (var i = 0; i < elements.length; i++) {

        var element = elements[i];
        var type = getSpecificType(element);
        var name = element.attr('text/text');

        var cid = element.cid;
        var id = cid;
		/*var id = cid.match(/\d+/g)[0];
        var numID = new Number(id);*/
		var position=element.attributes.position;

        switch (type) {
            case "entity":
				name=element.attributes.attrs.label.text;
				var entity='{"name":"'+prefix_Url_Ontology+name+'","ref":"","id":["'+id+'"],"type":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
				entities.push(entity);
                break;
            case "entityRefMode":
                name=element.attributes.attrs.label.text;
				var refMode_Name=element.attributes.attrs.refMode.text.replace('(','');
				refMode_Name=refMode_Name.replace(')','');
				var entity='{"name":"'+prefix_Url_Ontology+name+'","ref":"'+refMode_Name+'","id":["'+id+'"],"type":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
				entities.push(entity);
                break;
            case "value":
                name=element.attributes.attrs.label.text;
				var entity='{"name":"'+prefix_Url_Ontology+name+'","ref":"","id":["'+id+'"],"type":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
				entities.push(entity);
                break;
			case "unaryFactType":
				var connectedLinks=graph.getConnectedLinks(element);
				name=element.attr('role_name/text');
				var entitiesRelated=[];
				var uniquenessConstraints='';
				var mandatory=[];
				var leftRead=false;
				if(element.attr('left_read/display')=='inline') leftRead=true;

				uniquenessConstraints='"0..1"';
				jQuery.each( connectedLinks, function( i, val ) {
					var elto=graph.getCell(graph.getCell(val.cid).target().id);
					entitiesRelated.push('"'+prefix_Url_Ontology+elto.attr('label/text')+'"');
					graph.getCell(val.cid).attr('line/targetMarker/display')=='inline'? mandatory.push('"'+prefix_Url_Ontology+elto.attr('label/text')+'"'):"";
				});

				var relationship='{"name":"'+prefix_Url_Ontology+name+'","inverseReading":'+leftRead+',"entities":['+entitiesRelated+'],"uniquenessConstraints":['+uniquenessConstraints+'],"mandatory":['+mandatory+'],"type":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
				relationships.push(relationship);
                break;
			case "binaryFactType":
				var connectedLinks=graph.getConnectedLinks(element);
				name=element.attr('role_name/text');
				var entitiesRelated=[];
				var cardinality=element.attributes.cardinality;
				var uniquenessConstraints='';
				var mandatory=[];
				var leftRead=false;
				if(element.attr('left_read/display')=='inline') leftRead=true;

				switch (cardinality)
				{
					case 'oto':	uniquenessConstraints='"0..1","0..1"';	break;
					case 'otm':	uniquenessConstraints='"0..1","0..*"';	break;
					case 'mto':	uniquenessConstraints='"0..*","0..1"';	break;
					case 'mtm':	uniquenessConstraints='"0..*","0..*"';	break;
				}
				jQuery.each( connectedLinks, function( i, val ) {

					var elto=graph.getCell(graph.getCell(val.cid).target().id);
					//REALIZO EL CONTROL SOBRE QUE PORT DEL ROLE ESTA CONECTADO (LEFT O RIGHT)
					//DEBIDO A QUE AL  MOMENTO DE GRAFICAR EL JSON, EL ARRAY ENTITIES [ENTITY_A,ENTITY_B] TOMA POR DEFECTO EL IZQUIERDO COMO LEFT Y
					//EL DERECHO COMO RIGHT. GRAFICA EN ESE ORDEN
					if(graph.getCell(val.cid).source().port=='left')
					{
						entitiesRelated.unshift('"'+prefix_Url_Ontology+elto.attr('label/text')+'"');
					}
					else
					{
						entitiesRelated.push('"'+prefix_Url_Ontology+elto.attr('label/text')+'"');
					}

					graph.getCell(val.cid).attr('line/targetMarker/display')=='inline'? mandatory.push('"'+prefix_Url_Ontology+elto.attr('label/text')+'"'):"";
				});

				var relationship='{"name":"'+prefix_Url_Ontology+name+'","inverseReading":'+leftRead+',"entities":['+entitiesRelated+'],"uniquenessConstraints":['+uniquenessConstraints+'],"mandatory":['+mandatory+'],"type":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
				relationships.push(relationship);
                break;
			case "union":
			case "exclusive":
			case "exclusiveExhaustive":
			case "equality":

				if(type=="exclusiveExhaustive")
					type= 'exclusive","union';

				var idsCellsInvolved=[];
				var connectedLinks=graph.getConnectedLinks(element);
				var parentNameSubtyping='';
				var childEntities=[];
				var factTypes=[];
				var factPositions=[];

				if(connectedLinks.length>0)
				{
					jsonCountSubtyping++;
					//VERIFICO SI ES UNA CONTRAINT ENTRE ISA O ROLES.
					var linkElement=graph.getCell(connectedLinks[0].target().id);
					//alert(linkElement.prop('type'));
					if(linkElement.prop('type')=='orm.ISA')
					{
						//TOMO POR DEFECTO QUE TODOS LOS PADRES DEL ISA VAN HACER LOS MISMOS
						//alert(graph.getCell(linkElement.target().id).attr('label/text'))
						parentNameSubtyping=graph.getCell(linkElement.target().id).attr('label/text');
						idsCellsInvolved.push('"'+graph.getCell(linkElement.target().id).cid+'"');
						jQuery.each( connectedLinks, function( i, val ) {
							var l_e=graph.getCell(val.target().id);
							var l_e_entityChildName=graph.getCell(l_e.source().id).attr('label/text');
							idsCellsInvolved.push('"'+graph.getCell(l_e.source().id).cid+'"');
							childEntities.push('"'+prefix_Url_Ontology+l_e_entityChildName+'"');
						});
						var link='{"name":"'+prefix_Url_Ontology+'S'+jsonCountSubtyping+'","parent":"'+prefix_Url_Ontology+parentNameSubtyping+'","entities":['+childEntities+'],"id":['+idsCellsInvolved+'],"type":"subtyping","subtypingContraint":["'+type+'"],"position":{"x":'+position.x+',"y":'+position.y+'}}';
						links.push(link);
					}
					else if(roles_types.includes(linkElement.prop('type')))
					{
						jQuery.each( connectedLinks, function( i, val ) {
							var l_e=graph.getCell(val.target().id);
							var l_e_factPosition=val.target().port;
							var l_e_factTypeName=l_e.attr('role_name/text');
							factTypes.push('"'+prefix_Url_Ontology+l_e_factTypeName+'"');
							factPositions.push('"'+prefix_Url_Ontology+l_e_factPosition+'"');
						});
						var link='{"name":"'+prefix_Url_Ontology+'S'+jsonCountSubtyping+'","parent":"","factTypes":['+factTypes+'],"factPosition":['+factPositions+'],"id":'+id+', "type":"roleConstraint","roleContraint":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
						links.push(link);
					}
				}
                break;
			case "subset":

				var connectedLinks=graph.getConnectedLinks(element);
				var parentName='';
				var childEntities=[];
				var factTypes=[];
				var factParentTypes=[];
				var factPositions=[];
				var factParentPositions=[];

				if(connectedLinks.length>0)
				{
					jsonCountSubtyping++;
					//VERIFICO SI ES UNA CONTRAINT ENTRE ISA O ROLES.
					var linkElement=graph.getCell(connectedLinks[0].target().id);
					//alert(linkElement.prop('type'));
					if(roles_types.includes(linkElement.prop('type')))
					{
						jQuery.each( connectedLinks, function( i, val ) {

							var l_e=graph.getCell(val.target().id);
							var l_e_factPosition=val.target().port;
							var l_e_factTypeName=l_e.attr('role_name/text');

							if(val.prop('type')=='orm.DashedLinkArrow')
							{
								factParentTypes.push('"'+prefix_Url_Ontology+l_e_factTypeName+'"');
								factParentPositions.push('"'+prefix_Url_Ontology+l_e_factPosition+'"');
							}
							else
							{
								factTypes.push('"'+prefix_Url_Ontology+l_e_factTypeName+'"');
								factPositions.push('"'+prefix_Url_Ontology+l_e_factPosition+'"');
							}

						});
						var link='{"name":"'+prefix_Url_Ontology+'S'+jsonCountSubtyping+'","factParent":['+factParentTypes+'],"factTypes":['+factTypes+'],"factParentPosition":['+factParentPositions+'],"factPosition":['+factPositions+'], "type":"roleConstraint","roleContraint":"'+type+'","position":{"x":'+position.x+',"y":'+position.y+'}}';
						links.push(link);
					}
				}

                break;

        }
    }

	var allLinks = graph.getLinks();
	jQuery.each( allLinks, function( i, val ) {
		var idsCellsInvolved=[];
		if(val.prop('type')=='orm.ISA')
		{
			var childEntities=[];
			//ALL ISA WITHOUT CONTRAINTS BETWEEN ISA TYPES
			if(graph.getConnectedLinks(val).length==0)
			{
				jsonCountSubtyping++;

				idsCellsInvolved.push('"'+graph.getCell(val.target().id).cid+'"');
				idsCellsInvolved.push('"'+graph.getCell(val.source().id).cid+'"');

				parentNameSubtyping=graph.getCell(val.target().id).attr('label/text');
				var l_e_entityChildName=graph.getCell(val.source().id).attr('label/text');
				childEntities.push('"'+prefix_Url_Ontology+l_e_entityChildName+'"');
				var link='{"name":"'+prefix_Url_Ontology+'S'+jsonCountSubtyping+'","parent":"'+prefix_Url_Ontology+parentNameSubtyping+'","entities":['+childEntities+'],"id":['+idsCellsInvolved+'],"type":"subtyping","subtypingContraint":[]}';
				links.push(link);
			}
		}

	});
	return [entities,relationships,attributes,inheritances,links];
}

//CREAT A JSON OF THE GRAPH
function createJSON() {
	jsonCountSubtyping=0;
    var allElement = getAllElement();
    var entities = allElement[0];
    var relationships = allElement[1];
    var attributes = allElement[2];
    var inheritances = allElement[3];
    var connectors = allElement[4];
    var elements = graph.getElements();

	var json = '{"entities": ['+entities+'],"relationships":['+relationships+'],"attributes":['+attributes+'],"inheritances":['+inheritances+'],"connectors":['+connectors+']}';

	return json;
}

//FUNCTION THAT REMOVE ALL ELEMENTS OF THE GRAPH
function clearGraph(){
	hideElementTools();
	graph.removeCells(graph.getCells());
}

//CHANGE TYPE CONSTRAINT AFTER BEEN DRAWN.
function changeConstraintType(css_value){
	var elto=graph.getCell($('#cid').text());
	var elto_pos=elto.position();
	//alert(elto);
	var view_a=paper.findViewByModel(elto);

	var tipos=["orm.Union","orm.ExclusiveExhaustive","orm.Exclusive","orm.Equality","orm.Subset"];

	if ($.inArray(view_a.model.attributes.type, tipos)>=0)
	{
		//alert(view_a);
		var outboundLinks = graph.getConnectedLinks(elto);
		//alert(outboundLinks);
		switch($('#ddl_disj').val()) {
			case 'D1':
				var nuevo_elto= new orm.Union({});
			break;

			case 'D2':
				var nuevo_elto= new orm.ExclusiveExhaustive({});
			break;

			case 'D3':
				var nuevo_elto= new orm.Exclusive({});
			break;

			case 'D4':
				var nuevo_elto= new orm.Equality({});
			break;

			case 'D5':
				var nuevo_elto= new orm.Subset({});
			break;
		}

		graph.addCell(nuevo_elto);
		nuevo_elto.position(elto_pos.x,elto_pos.y);

		jQuery.each( outboundLinks, function( i, val ) {
			if((val.source().id) ==graph.getCell($('#cid').text()).id)
			{
				val.source(nuevo_elto);
			}

			if((val.target().id) ==graph.getCell($('#cid').text()).id)
			{
				val.target(nuevo_elto);
			}
		});
		graph.removeCells(elto);
		$('#cid').text((paper.findViewByModel(nuevo_elto)).model.cid);
	}
}

//FUNCTION THAT REMARK AN ARRAY OF ELEMENTS PASS AS ARGUMENT
function remarkWrongObjects(arrayObject){
	$(arrayObject).each(function() {
		var cellView=paper.findView($(this));
		cellView.highlight(null, myHighlighterWrong);
	});
}

//SETS BACKGROUND COLOR OF THE DROPDOWNLIST OF COLORS
function changeColor(selectElement){
	var paperBgColor=$(selectElement).val();
	$(selectElement).css('background-color', paperBgColor);
}

function changeRange(element,idTarget) {
	$('#'+idTarget).val($(element).val());
}

//ABORT CHANGES
function cancelChanges(idPalleteAttr){
	$('#'+idPalleteAttr).addClass("invisible");
}

//SHOWS THE PAPER PALLETE OPTIONS
function showPaperOptions(){
	$('#paletteOptions').addClass('invisible');
	$('#palettePaperOptions').removeClass('invisible');
}

/////////////////////////////////////////////////////////////////////////////////
//LOAD JSON TO BE GRAPH
function loadJson(btn){
	if(document.getElementById('FileInput'))
	{
		mostrarLoading();
		var btn_submit= document.getElementById('FileInput');
		btn_submit.click();
		document.getElementById('FileInput').addEventListener('change', onChange_FileInput);
		$(btn).unbind('click');
		closeLoading();
	}
}

function onChange_FileInput(event) {
	mostrarLoading();
	$('#FileInput').unbind('onchange');
    var reader = new FileReader();
    reader.onload = onReaderLoad;
    reader.readAsText(event.target.files[0]);
}

function onReaderLoad(event){
	//ine:2240');
	associated_cids=[];
	associated_name_cids=[];
    var obj = JSON.parse(event.target.result);

	graphJson_Entities(obj.entities);
	graphJson_Relationships(obj.relationships);
	graphJson_Connectors(obj.connectors);
	closeLoading();
	document.getElementById("FileInput").value = "";
}

function graphJson_Entities(entities){
	jQuery.each(entities, function( i, val ) {
		switch (val.type) {
			case "entity":
				var entity_value = new orm.Entity();
				var arrayUri=val.name.split('#');
				entity_value.attr('label/text',arrayUri[arrayUri.length -1]);
				entity_value.attributes.uri=val.name;

				if(val.position)
					entity_value.position(val.position.x,val.position.y);
				else
					setRndPosition(entity_value);

				graph.addCell(entity_value);
				resizeElementFromLabel(paper.findViewByModel(entity_value));
				associated_name_cids.push('"'+val.name+'":"'+entity_value.cid+'"');

				break;
			case "entityRefMode":
				var entity_value = new orm.EntityRefMode();
				var arrayUri=val.name.split('#');
				entity_value.attr('label/text',arrayUri[arrayUri.length -1]);
				entity_value.attributes.uri=val.name;
				if(val.ref)
					entity_value.attr('refMode/text','('+val.ref+')');

				if(val.position)
					entity_value.position(val.position.x,val.position.y);
				else
					setRndPosition(entity_value);

				graph.addCell(entity_value);
				resizeElementFromLabel(paper.findViewByModel(entity_value));
				associated_name_cids.push('"'+val.name+'":"'+entity_value.cid+'"');
				break;
			case "value":
				var entity_value = new orm.Value();
				var arrayUri=val.name.split('#');
				entity_value.attr('label/text',arrayUri[arrayUri.length -1]);
				entity_value.attributes.uri=val.name;

				if(val.position)
					entity_value.position(val.position.x,val.position.y);
				else
					setRndPosition(entity_value);

				graph.addCell(entity_value);
				resizeElementFromLabel(paper.findViewByModel(entity_value));
				associated_name_cids.push('"'+val.name+'":"'+entity_value.cid+'"');
				break;
	}});
}

function createLinkBetweenCells(type, cellParent, cellChild){
	var notFound=true;
	var newLink;
	var outBoundLinks=graph.getConnectedLinks(cellChild, { outbound: true });
	jQuery.each( outBoundLinks, function( i, val ) {
		//alert(val.prop('type') +" Parent:" + cellParent.cid + " Target:" + graph.getCell(val.target().id).cid);
		if( graph.getCell(val.target().id).cid==cellParent.cid && val.prop('type')=="orm.ISA")
		{
			notFound=false;
			newLink=val;
			return;
		}
	});

	//alert("notFound:"+notFound);
	if(notFound)
	{
		switch (type)
		{
			case "ISA":
				newLink =new joint.shapes.orm.ISA();
				newLink.source(cellChild);
				newLink.target(cellParent);
				newLink.addTo(graph);
				add_hide_Tools(newLink);
				break;
		}
	}
	return newLink;
}

function getCss_uniquenessConstraints(uniquenessConstraints)
{
	//alert(uniquenessConstraints);
	var returnValue='';
	var stringValue=uniquenessConstraints[0]+','+uniquenessConstraints[1];
	//alert(stringValue);
	switch (stringValue)
	{
			case "0..*,0..*":
				returnValue='mtm';
				break;
			case "0..*,0..1":
			case "0..*,1..1":
				returnValue='mto';
				break;
			case "0..1,0..*":
			case "1..1,0..*":
				returnValue='otm';
				break;
			case "0..1,0..1":
			case "1..1,1..1":
				returnValue='oto';
				break;
	}

	//alert('RETURN CARDINALITY:'+returnValue);
	return returnValue;
}

function add_hide_Tools(element)
{
	var element_View=element.findView(paper);
	element_View.addTools(createToolView());
	element_View.hideTools();
}

function graphJson_Relationships(relationships){
	//alert(associated_cids);
	var a_cids=JSON.parse("{"+associated_cids+"}");
	var a_name_cids=JSON.parse("{"+associated_name_cids+"}");
	jQuery.each(relationships, function( i, val ) {
		switch (val.type) {
			case "unaryFactType":
				var mandatories=val.mandatory;
				var arrayUri=val.name.split('#');
				var u_role= new orm.Role_1({
				attrs:{'role_name': {text: arrayUri[arrayUri.length -1]}},
				ports: { items: [{ id:'left',group: 'left' }]}});

				u_role.attributes.uri=val.name;
				associated_name_cids.push('"'+val.name+'":"'+u_role.cid+'"');

				if(val.position)
					u_role.position(val.position.x,val.position.y);
				else
					setRndPosition(u_role);

				if(val.inverseReading)
				{
					var new_reading=val.inverseReading;
					(new_reading==1) ? u_role.attr('left_read/display', 'inline') : u_role.attr('left_read/display', 'none');
					u_role.attributes.inverseReading=new_reading;
				}

				graph.addCell(u_role);

				var new_link_L = new joint.shapes.orm.CustomLink({
					source: {
						id: u_role.id,
						port: 'left'
					}
				});
				var cell_target=graph.getCell(a_name_cids[val.entities[0]]);

				if( mandatories.includes(cell_target.findView(paper).model.attributes.uri))
					new_link_L.attr('line/targetMarker/display','inline');
				new_link_L.target(cell_target);

				graph.addCells(new_link_L);
				add_hide_Tools(new_link_L);


				break;
			case "binaryFactType":

				var mandatories=val.mandatory;
				var arrayUri=val.name.split('#');

				var b_role= new orm.Role_2({
				attrs:{'role_name': {text: arrayUri[arrayUri.length -1]}},
				ports: { items: [{ id:'left',group: 'left' },{id:'center', group: 'center'},{id:'right', group: 'right'}]}});

				b_role.attributes.uri=val.name;
				associated_name_cids.push('"'+val.name+'":"'+b_role.cid+'"');

				if(val.position)
					b_role.position(val.position.x,val.position.y);
				else
					setRndPosition(b_role);

				if(val.inverseReading)
				{
					var new_reading=val.inverseReading;
					(new_reading==1) ? b_role.attr('left_read/display', 'inline') : b_role.attr('left_read/display', 'none');
					b_role.attributes.inverseReading=new_reading;
				}

				if(val.uniquenessConstraints)
				{
					var newUniquenessConstraints=getCss_uniquenessConstraints(val.uniquenessConstraints);
					b_role.attr(b_role.attributes.cardinality+'/display', 'none');
					b_role.attr(newUniquenessConstraints+'/display', 'inline');
					b_role.attributes.cardinality=newUniquenessConstraints;//
				}

				graph.addCell(b_role);

				var new_link_L = new joint.shapes.orm.CustomLink({
					source: {
						id: b_role.id,
						port: 'left'
					}
				});
				var cell_target=graph.getCell(a_name_cids[val.entities[0]]);

				if( mandatories.includes(cell_target.findView(paper).model.attributes.uri))
					new_link_L.attr('line/targetMarker/display','inline');
				new_link_L.target(cell_target);

				var new_link_R = new joint.shapes.orm.CustomLink({
					source: {
						id: b_role.id,
						port: 'right'
					}
				});
				cell_target=graph.getCell(a_name_cids[val.entities[1]]);
				if( mandatories.includes(cell_target.findView(paper).model.attributes.uri))
					new_link_R.attr('line/targetMarker/display','inline');
				new_link_R.target(cell_target);

				graph.addCells(new_link_L,new_link_R);
				add_hide_Tools(new_link_L);
				add_hide_Tools(new_link_R);

				break;
			}
	});




}

function graphJson_Connectors(connectors){
	//alert(associated_cids);

	var a_cids=JSON.parse("{"+associated_cids+"}");
	var a_name_cids=JSON.parse("{"+associated_name_cids+"}");
	jQuery.each(connectors, function( i, val ) {
		switch (val.type) {
			case "subtyping":

				var subtypingConversion="";
				var bandFirst=true;
				jQuery.each(val.subtypingContraint, function( i, val ) {
					if(bandFirst)
						subtypingConversion=val;
					else
						subtypingConversion=subtypingConversion+','+val;
					bandFirst=false;
						});

				switch (subtypingConversion) {
					case "":
						var new_link_R = new joint.shapes.orm.ISA();

						new_link_R.target(graph.getCell(a_name_cids[val.parent]));
						new_link_R.source(graph.getCell(a_name_cids[val.entities[0]]));
						new_link_R.addTo(graph);
						add_hide_Tools(new_link_R);

						break;

					case "union":
					case "exclusive":
					case "exclusive,union":
					case "union,exclusive":

						if( subtypingConversion=="exclusive,union" || subtypingConversion== "union,exclusive")
							subtypingConversion="exclusiveExhaustive";

						var newContraint=createNewElementConstraint(subtypingConversion,graph,val.position);
						//alert();
						var parentId=a_name_cids[val.parent];
						$.each(val.entities, function( index, value ) {
							var new_link_ISA = createLinkBetweenCells("ISA", graph.getCell(parentId), graph.getCell(a_name_cids[val.entities[index]]));
							var new_link = new joint.shapes.orm.DashedLink();
							new_link.source(newContraint);
							new_link.target(new_link_ISA);
							new_link.addTo(graph);

							add_hide_Tools(new_link);

						});

					break;
				}
			break;

	}});
}

function saveJson(){
		//create JSON
		var myJSON = createJSON();
		// create a link DOM fragment
		var $link = $("<a/>");
		// encode any special characters in the JSON
		var text = encodeURIComponent( myJSON );

		$link
		.attr( "download", "myGraph.json" )
		.attr( "href", "data:application/octet-stream," + text )
		.appendTo( "body" )
		.get(0)
		.click();

		closeLoading();
	}


  var graphTreeLayout = new joint.layout.TreeLayout({
    graph: graph,
    parentGap: 50,
    siblingGap: 50
  });


  var graphForceLayout = new joint.layout.ForceDirected({
    graph: graph,
    width: 600,
    height: 400,
    gravityCenter: { x: 300, y: 200 },
    charge: 180,
    linkDistance: 30
  });

  //http://www.daviddurman.com/automatic-graph-layout-with-jointjs-and-dagre.html
  var graphDirected = new joint.layout.DirectedGraph.layout(graph,{
     nodeSep: 100,
     edgeSep: 200,
     rankDir: "BT"
    });


/////////////////////////////////////////////////////////////////////////////////
