var ServerConnection, exports;

function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
  		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
ServerConnection = class ServerConnection {
  // @param error_callback {function} which error callback function to use.
  constructor(error_callback) {
    this.error_callback = error_callback;
    this.urlprefix = "../";
  }

  set_urlprefix(str) {
    return this.urlprefix = str;
  }

	request_reasoning_enzo(json) {
		var postdata, url;
		url = this.urlprefix + "reasoning/querying/satisfiableORM.php";
		//alert(url);
		var ajax = objetoAjax();
		ajax.open("POST",url,true);

		ajax.onreadystatechange=function() {
			if (ajax.readyState == 4) {

				var obj = jQuery.parseJSON(ajax.responseText);

				var unsatisfiabilityKB=obj.satisfiable.kb;
				var unsatisfiableClasses=obj.satisfiable.classes;

				remarkUnsatisfiableClasses(graph, obj.unsatisfiable.classes);
				printKB_result($("#guideFeedback"), obj.satisfiable.kb, obj.unsatisfiable.classes);
				closeLoading();
				}
			}
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send('json='+json);
	}

	request_encoding_enzo(json){

		  var postdata, url;
		url = this.urlprefix + "reasoning/translate/crowdOrm.php";

		var ajax=objetoAjax();
		ajax.open("POST",url,true);
		ajax.onreadystatechange=function() {

			if (ajax.readyState==4) {

				//alert(ajax.responseText);
				console.log(ajax.responseText);
				$('#modalBodyContent').text(ajax.responseText);
				$('#modal_Contenido').modal('show');
				closeLoading();

				}
			}

		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send('json='+json);
	}


}

function encodingEnzoServer(json){
	var res;
	res=ServerConnection.request_encoding_enzo(json);
}

function reasoningEnzoServer(json){
	var res;
	res=ServerConnection.request_reasoning_enzo(json);
}


exports = exports != null ? exports : this;
exports.ServerConnection = new ServerConnection;
