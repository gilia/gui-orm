
//Get as parameters GRAPH (PRINCIPAL) and Those unsatisfiable classes asn an ARRAY having defauls URIS
function remarkUnsatisfiableClasses(graph,unsatisfiableClasses)
{
  var cells=graph.getCells();
  var cv;
  var uriName='';
  cells.forEach(function(c){
    if(typeof c.attributes.uri !== "undefined")
    {
      cv=c.findView(paper);
      cv.unhighlight(null, myHighlighterWrong);
      uriName=c.attributes.uri;
      if(unsatisfiableClasses.includes(uriName))
      {
        cv.highlight(null, myHighlighterWrong);
      }
    }
  });

}

//shows leyend (KB satisfiability) in INPUT given.
function printKB_result(input,KB_Satisfiability,unsatisfiableClasses)
{
  var kbResult="";
  var classesResult="";
  if($(unsatisfiableClasses).length>0)
    classesResult="but some classes don't";
  if(KB_Satisfiability)
    kbResult="KB is SATISFIABLE " + classesResult;
  else
    kbResult="KB is NOT SATISFIABLE";
  $(input).val(kbResult);

}
