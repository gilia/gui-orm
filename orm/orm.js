// herramientas para la paleta
var fixedPalette = true;
var opacityPalette = false;
var extendedPalette = false;
var horizontalPalette = false;
var onSelectionColor= 'DodgerBlue';
//DEFINICION DE ATRIBUTOS MEDIANTE VARIABLES PARA ENTIDADES Y SUS VARIANTES
var original_width_entities=90;
var original_height_entities=35;
var original_size_label_entities=14;
var original_strokeWidth_entities=2;
var original_strokeFill_entities='#0000AD';
var original_rx_entities=5;
var original_ry_entities=5;
var original_fill_entities='#FFFFFF';
var original_dasharray_entities='6 3';
//DEFINICION DE ATRIBUTOS MEDIANTE VARIABLES CONSTRAINTS
var original_strokeWidth_constraint=2;
var original_rx_constraint=15;
var original_ry_constraint=15;
var original_cx_constraint=15;
var original_cy_constraint=15;
var original_fill_constraint='#FFFFFF';
var original_strokeFill_constraint='#A000A0';
//DEFINICION DE ATRIBUTOS MEDIANTE VARIABLES PARA ROLES
var original_width_role=28;
var original_height_role=17;
var original_rx_role=3;
var original_ry_role=3;
var original_fill_role='#FFFFFF';
var original_strokeFill_role='#333333';

var htmlHexColors=['#FFFFFF','#FAFAFA','#F2F2F2','#E6E6E6','#D8D8D8','#BDBDBD','#A4A4A4',
'#EFF8FB','#E0F8F7','#CEF6F5','#A9F5F2','#E0F8E0','#CEF6CE','#A9F5A9','#F2F5A9','#F3F781','#F4FA58',
'#F7FE2E','#FFFF00','#F7BE81','#FAAC58','#FE9A2E','#FFBF00'];


//VARIABLES UTILIZADAS EN FUNCIONES
var min_px_char=16;
var prefix_Url_Ontology="http://crowd.fi.uncoma.edu.ar#";
var jsonCountSubtyping=0;
var associated_cids=[];

//ARRAYS OF TYPES
var roles_types = ["orm.Role_1", "orm.Role_2", "orm.Role_3"];

//ELEMENTOS ORM
joint.dia.Element.define('orm.Role_1', {
	
    size: { width: original_width_role, height: original_height_role },
	inverseReading:0,
	overHeadRoleName:0,
    attrs: {
        'inner': {
			class:'highlightable',
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: original_strokeFill_role,
			//refX: '100%',
			rx:original_rx_role,
			ry:original_ry_role,
            fill: original_fill_role
         },
        'oto': {
			'x1':2,
			'y1':-7 ,
			'x2':25,
			'y2':-7,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
        },
        'role_name': {
			text: 'role name',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: original_strokeFill_role
        },
		   'background_role_name': {
			ref: 'role_name',

			//display:'none',
			fill:'#ffffff',
			refHeight:'100%',
			refWidth:'100%',
			refX: '0%',
			refY: '0%'
         },
		'left_read': {
			ref: 'role_name',
			refX: '-13px',
			refY: '20%',
			'display':'none',
			//refY: '40%',
            fill: original_strokeFill_role

        },
        root: { magnet: false }
    },
	 ports: {
			groups: {
				'left': {
					attrs: {
						portBody: {
								magnet: 'passive',
								event: 'element:Role_2:pointerup',
								width:original_width_role,
								height:original_height_role,
								x:0,
								y:-9,
								rx:3,
								ry:3,
								strokeWidth: 1,
								/*anda pero voy por otro camino estetico
								stroke: 'transparent', fill: 'transparent',
								stroke: '#58FA58', fill: '#9FF781',
								opacity:'0.5'
								*/
								stroke: original_strokeFill_role, fill:onSelectionColor,


							}
					},
					position: { name: 'left' },
					label: { position: { name: 'left' } },

					markup: [{
						tagName: 'rect',
						selector: 'portBody',
						}],
					 z: -1
				},
			}
		}
	},	{
	 markup: [{
        tagName: 'rect',
        selector: 'inner',
    }, {
        tagName: 'line',
        selector: 'oto'
    }
	, {
        tagName: 'rect',
        selector: 'background_role_name'
    }
	, {
        tagName: 'text',
        selector: 'role_name'
    }
	, {
            tagName: 'path',
            selector: 'left_read',
            attributes: {
                'd': 'M0 5 L8 0 L8 10 Z',
                'fill': original_strokeFill_role,
                'stroke': original_strokeFill_role,
                'stroke-width': 1,
                'pointer-events': 'none'
            }
        }]


});

joint.dia.Element.define('orm.Role_2', {
    size: { width: original_width_role*2, height: original_height_role},
	cardinality:'oto',
	inverseReading:0,
	overHeadRoleName:0,
    attrs: {
        'inner': {
			class:'highlightable',
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: original_strokeFill_role,
			//refX: '100%',
			rx:original_rx_role,
			ry:original_ry_role,
            fill: original_fill_role
        },
        'line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':16,
			strokeWidth: 2,
			stroke: '#333',
			refX: '50%'
         },
        'mtm': {
			'x1':2,
			'y1':-7 ,
			'x2':53,
			'y2':-7,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'

        },
        'mto': {
			'x1':2,
			'y1':-7 ,
			'x2':24,
			'y2':-7,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'

        },
        'otm': {
			'x1':30,
			'y1':-7 ,
			'x2':53,
			'y2':-7,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'

        },
        'oto': {

			'x1':2,
			'y1':-7 ,
			'x2':53,
			'y2':-7,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'stroke-dasharray':'23 7',
			'display':'inline',
			stroke: '#8A0868'
        },
        'role_name': {
			text: 'role name',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'

        },
        'background_role_name': {
			ref: 'role_name',

			//display:'none',
			fill:'#ffffff',
			refHeight:'100%',
			refWidth:'100%',
			refX: '0%',
			refY: '0%'
         },
		'left_read': {
			ref: 'role_name',
			refX: '-13px',
			refY: '20%',
			'display':'none',
			//refY: '40%',
            fill: '#333333'

        },
		'line_Right_const': {
			'x1':32,
			'y1':21 ,
			'x2':37,
			'y2':50,
			opacity:'0.8',
			strokeWidth: 1,
			stroke: 'blue',
			refX: '50%',
			'display':'none',
			'stroke-dasharray':'4 4'
         },
		 'line_Left_const': {
			'x1':-32,
			'y1':21 ,
			'x2':-37,
			'y2':50,
			opacity:'0.8',
			strokeWidth: 1,
			stroke: 'blue',
			refX: '50%',
			'display':'none',
			'stroke-dasharray':'4 4'
         },
		  'ifc_Right_const': {
			ref:'line_Right_const',
			text: '1..54',
			'font-family': 'Arial',
			textVerticalAnchor: 'middle',
			textAnchor: 'middle',
			refX: '60%',
			'font-weight':'bold',
			y: 60,
			fontSize: 12,
			'display':'none',
			fill: 'blue'

        },
		 'ifc_Left_const': {
			ref:'line_Left_const',
			text: '1..54',
			'font-family': 'Arial',
			textVerticalAnchor: 'middle',
			textAnchor: 'middle',
			refX: '40%',
			'font-weight':'bold',
			y: 60,
			fontSize: 12,
			'display':'none',
			fill: 'blue'

        },

        root: { magnet: false }
     },

	 ports: {
        groups: {
			'left': {
					attrs: {
						portBody: {
								magnet: 'passive',
								event: 'element:Role_2:pointerup',
								width:original_width_role,
								height:original_height_role,
								x:0,
								y:-9,
								rx:2,
								ry:2,
								strokeWidth: 1,
								/*anda pero voy por otro camino estetico
								stroke: 'transparent', fill: 'transparent',
								stroke: '#58FA58', fill: '#9FF781',
								opacity:'0.5'
								*/
								stroke: '#333', fill: onSelectionColor,


							}
					},
					position: { name: 'left' },
					label: { position: { name: 'left' } },

					markup: [{
						tagName: 'rect',
						selector: 'portBody',
						}],
					  z: -1,
				},

            'right': {
                 markup: [{
					tagName: 'rect',

					selector: 'portBody',
					attributes: {
						width:original_width_role,
						height:original_height_role,
						x:-28,
						y:-9,
						rx:2,
						ry:2,
						strokeWidth: 0	,
						/*anda pero voy por otro camino estetico
						stroke: 'transparent', fill: 'transparent',
						stroke: '#58FA58', fill: '#9FF781',
						opacity:'0.5'
						*/
						stroke: '#333',
						fill: onSelectionColor,

						}
						}],
                z:-1,
                attrs: { portBody: { magnet: 'passive',event: 'element:Role_2:pointerup'} },

                position: { name: 'right' },
                label: { position: { name: 'right' } }
            }
			,
            'center': {
                 markup: [{
					tagName: 'rect',

					selector: 'portBody',
					attributes: {
						width:original_width_role/2,
						height:original_height_role,
						x:original_width_role-(original_width_role/4),
						y:-9,
						rx:2,
						ry:2,
						strokeWidth: 0,
						stroke: '#333',
						fill: onSelectionColor,

						}
						}],
                z:-1,
                attrs: { portBody: { magnet: 'passive',event: 'element:Role_2:pointerup'} },

                position: { name: 'center' },
                label: { position: { name: 'center' } }
            }
        }
    }
}, {

	markup: [{
        tagName: 'rect',
        selector: 'inner',
    }, {
        tagName: 'line',
        selector: 'oto'
    }
	, {
        tagName: 'line',
        selector: 'otm'
    }
	, {
        tagName: 'line',
        selector: 'mto'
    }
	, {
        tagName: 'line',
        selector: 'mtm'
    }
	, {
        tagName: 'line',
        selector: 'line_1'
    }
	, {
        tagName: 'line',
        selector: 'line_Left_const'
    }
	, {
        tagName: 'line',
        selector: 'line_Right_const'
    }

	, {
        tagName: 'rect',
        selector: 'background_role_name'
    }
	, {
        tagName: 'text',
        selector: 'role_name'
    }
	, {
        tagName: 'text',
        selector: 'ifc_Right_const'
    }
	, {
        tagName: 'text',
        selector: 'ifc_Left_const'
    }
	, {
            tagName: 'path',
            selector: 'left_read',
            attributes: {
                'd': 'M0 5 L8 0 L8 10 Z',
                'fill': '#333',
                'stroke': '#333',
                'stroke-width': 1,
                'pointer-events': 'none'
            }
        }]
    //markup: '<g class="rotatable scalable"> <rect class="inner"/><line class="line_1"/>	<line class="mtm"/>	<line class="mto"/>	<line class="otm"/>	<line class="oto"/>	</g><text class="role_name"/>',
});

joint.dia.Element.define('orm.Role_3', {
    size: { width: 105, height: 20 },
    attrs: {
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'

        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'

        },
		root: { magnet: false } // Disable the possibility to connect the body of our shape. Only ports can be connected.
    },

	 ports: {
        groups: {
            'left': {
                markup: [{
					tagName: 'rect',
					selector: 'portBody',
					attributes: {
						width:30,
						height:20,
						x:0,
						y:-10,
						rx:3,
						ry:3,

						strokeWidth: 2,
						stroke: '#333', fill: '#82FA58'
						//stroke: 'transparent', fill: 'transparent'

						}
						}],
                z: -1,
                attrs: { portBody: { magnet: false} },
                position: { name: 'left' },
                label: { position: { name: 'left' } }
            },
            'middle': {
                 markup: [{
					tagName: 'rect',
					selector: 'portBody',
					attributes: {
						width:27,
						height:20,
						x:-66,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						stroke: '#333', fill: '#A9BCF5'
						//stroke: 'transparent', fill: 'transparent'

						}
						}],
                z: -1,
                attrs: { portBody: { magnet: false } },
                position: { name: 'right' },
                label: { position: { name: 'right' } }
            },
            'right': {
                 markup: [{
					tagName: 'rect',
					selector: 'portBody',
					attributes: {
						width:30,
						height:20,
						x:-30,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						stroke: '#333', fill: '#F78181'
						//stroke: 'transparent', fill: 'transparent'

						}
						}],
                z: -1,
                attrs: { portBody: { magnet: false } },
                position: { name: 'right' },
                label: { position: { name: 'right' } }
            }
        }
    }




}, {
    markup: '<g class="rotatable scalable"> <rect class="inner"/><line class="line_1"/><line class="line_2"/><line class="line_up"/><text class="role_name"/></g>',
});

//----------------------------------ENTITY
joint.dia.Element.define('orm.Entity', {
	//size: { width: 100, height: 50 },
	class:'selected',
	uri:'',
	size: { width: original_width_entities, height: original_height_entities },
	attrs: {

        body: {
			event: 'element:Entity:pointerup',
			class:'highlightable',
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: original_strokeWidth_entities,
			rx:original_rx_entities,ry:original_ry_entities,
			stroke: original_strokeFill_entities,
			fill: original_fill_entities
         },
        label: {
			event: 'element:Entity:pointerup',
			text: 'Entity',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '50%',
            fontSize: original_size_label_entities,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }]
});
//----------------------------------FINAL ENTITY

//----------------------------------ENTITY REF MODE
joint.dia.Element.define('orm.EntityRefMode', {
	uri:'',
	size: { width: original_width_entities, height: original_height_entities+10 },
    attrs: {
        body: {
			class:'highlightable',
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: original_strokeWidth_entities,
			rx:original_rx_entities,ry:original_ry_entities,
			stroke: original_strokeFill_entities,
            fill: original_fill_entities
			},
        label: {
			text: 'Entity',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '30%',
            fontSize: original_size_label_entities,
            fill: '#333333'
        },
        refMode: {
			text: '(ref. mode)',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
			'font-weight':'bold',
            refX: '50%',
            refY: '70%',
            fontSize: original_size_label_entities,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }, {
        tagName: 'text',
        selector: 'refMode'
    }]
});
//----------------------------------FINAL ENTITY  REF MODE

//----------------------------------VALUE
joint.dia.Element.define('orm.Value', {
	uri:'',
	size: { width: original_width_entities, height: original_height_entities },
    attrs: {
        body: {
			class:'highlightable',
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: original_strokeWidth_entities,
			'stroke-dasharray':original_dasharray_entities,
			rx:original_rx_entities,ry:original_ry_entities,
			stroke: original_strokeFill_entities,
            fill: original_fill_entities
        },
        label: {
			text: 'Value',
            'font-family': 'Arial',
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '50%',
            fontSize: original_size_label_entities,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }]
});
//----------------------------------FINAL VALUE

//----------------------------------CONSTRAINTS
joint.dia.Element.define('orm.Union', {
    attrs: {
		external_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint,
			rx:original_ry_constraint,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_fill_constraint
        },
		internal_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint/2,
			rx:original_ry_constraint/2,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_strokeFill_constraint
        },

    }
}, {
	 markup: [{
		tagName: 'ellipse',
		selector: 'external_ellipse',
	}, {
		tagName: 'ellipse',
		selector: 'internal_ellipse',
	}]

});

joint.dia.Element.define('orm.ExclusiveExhaustive', {

	  attrs: {
		external_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint,
			rx:original_ry_constraint,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_fill_constraint
        },
		internal_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint/2,
			rx:original_ry_constraint/2,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_strokeFill_constraint
        },
		line1: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:24.98111,
			y2:24.92465,
			y1:4.71622,
			x1:4.85748,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        },
		line2: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:4.85748,
			y2:24.92465,
			y1:4.71622,
			x1:24.98111,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        }


    }
}, {
	 markup: [{
		tagName: 'ellipse',
		selector: 'external_ellipse',
	}, {
		tagName: 'ellipse',
		selector: 'internal_ellipse',
	}, {
		tagName: 'line',
		selector: 'line1',
	}, {
		tagName: 'line',
		selector: 'line2',
	}]

});

   //<line stroke="#A000A0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="-11.74654" y2="12.40336" y1="-12.57387" x1="12.87015" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="none"/>
//markup: '<g class="rotatable scalable"><ellipse cx="0" cy="0" id="svg_2" stroke="#A000A0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/>   <line stroke-linecap="null" stroke-linejoin="null" id="svg_3" x2="12.98111" y2="12.92465" y1="-11.71622" x1="-13.85748" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="none" stroke="#A000A0"/>   <line stroke="#A000A0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="-11.74654" y2="12.40336" y1="-12.57387" x1="12.87015" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="none"/>   <ellipse cx="0" cy="0" id="svg_2" stroke="#A000A0" ry="9" rx="9" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="#A000A0"/></g>'
joint.dia.Element.define('orm.Exclusive', {

    attrs: {
		external_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint,
			rx:original_ry_constraint,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_fill_constraint
        },
		line1: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:24.98111,
			y2:24.92465,
			y1:4.71622,
			x1:4.85748,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        },
		line2: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:4.85748,
			y2:24.92465,
			y1:4.71622,
			x1:24.98111,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        }


    }
}, {
	 markup: [{
		tagName: 'ellipse',
		selector: 'external_ellipse',
	}, {
		tagName: 'line',
		selector: 'line1',
	}, {
		tagName: 'line',
		selector: 'line2',
	}]

});

joint.dia.Element.define('orm.Equality', {

    attrs: {
		external_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint,
			rx:original_ry_constraint,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_fill_constraint
        },
		line1: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:7,
			y2:19,
			y1:19,
			x1:23,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        },
		line2: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:7,
			y2:12,
			y1:12,
			x1:23,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        }


    }
}, {
	 markup: [{
		tagName: 'ellipse',
		selector: 'external_ellipse',
	}, {
		tagName: 'line',
		selector: 'line1',
	}, {
		tagName: 'line',
		selector: 'line2',
	}]

});

//	markup: '<g class="rotatable scalable">   <ellipse id="svg_2" stroke="#A000A0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/>  <line stroke="#A000A0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="10" y2="4" y1="4" x1="-10" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/>  <line stroke="#A000A0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="10" y2="-4" y1="-4" x1="-10" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/> </g>'
joint.dia.Element.define('orm.Subset', {

    attrs: {
		external_ellipse: {
			cx:original_cx_constraint,
			cy:original_cy_constraint,
			stroke:original_strokeFill_constraint,
			ry:original_rx_constraint,
			rx:original_ry_constraint,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			fill:original_fill_constraint
        },
		simb: {
			text: 'U',
            'font-family': 'Arial',
			fill:original_strokeFill_constraint,
			stroke:original_strokeFill_constraint,
			'stroke-width':0,
			x:5,
			y:-10,
			fontSize:24,
			'text-anchor':'start',
			transform:'rotate(90 -1.8749999999999998,-2.5000000000000004)'


        },
		line2: {
			'stroke-linecap':'null',
			'stroke-linejoin':'null',
			x2:8,
			y2:23,
			y1:23,
			x1:23,
			'fill-opacity':'null',
			'stroke-opacity':'null',
			'stroke-width':original_strokeWidth_constraint,
			 stroke:original_strokeFill_constraint
        }
	}
}, {
	 markup: [{
		tagName: 'ellipse',
		selector: 'external_ellipse',
	}, {
		tagName: 'text',
		selector: 'simb',
	}, {
		tagName: 'line',
		selector: 'line2',
	}]

});

//	markup: '<g class="rotatable scalable">   <ellipse id="svg_2" stroke="#A000A0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/> <line stroke="#A000A0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="9" y2="9" y1="9" x1="-9" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/>  <text fill="#A000A0" stroke="#000000" stroke-width="0" x="-11.99954" y="6.9994" id="svg_11" font-size="28" font-family="Helvetica, Arial, sans-serif" text-anchor="start" xml:space="preserve" transform="rotate(90 -1.8749999999999998,-2.5000000000000004) ">U</text></g>'

//----------------------------------FIN CONSTRAINTS
joint.shapes.orm.ISA_Disj = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.ISA_Disj',
	attrs: {
	'.connection': { stroke: '#A000A0', 'stroke-width': 4 }
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	//'.marker-target': { stroke: '#A000A0', fill: '#A000A0', d: 'M 13 0 L 0 7 L 13 14 z' }
	}

    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.ISA_Double = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.ISA',
	attrs: {
	'.connection': { stroke: '#A000A0', 'stroke-width': 4 },
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	'.marker-target': { stroke: '#A000A0', fill: '#A000A0', d: 'M 13 0 L 0 7 L 13 14 z' }
	}

    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.Dashed_Link = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.Dashed_Link',
	attrs: {
	'.connection': { stroke: 'blue', 'stroke-width': 2,'stroke-dasharray': '7 3', 'fill': 'blue',    },

	'.marker-source': { stroke: '#A000A0', fill: '#A000A0', d: 'M 10 0 L 0 5 L 10 10 z','z-index': -1  },
	'.marker-target': { stroke: '#A000A0', fill: '#A000A0', d: 'M 13 0 L 0 7 L 13 14 z','z-index': -1  }

	}

    }, joint.dia.Link.prototype.defaults),



});

joint.shapes.orm.Link = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.Link',
	attrs: {
	'.connection': { stroke: '#A000A0', 'stroke-width': 2 },
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	//'.marker-target': { stroke: '#9100DA', fill: '#9100DA', d: 'M 13 0 L 0 7 L 13 14 z' }
	}

    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.Mandatory = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.Mandatory',
        attrs: {
	'.connection': { stroke: '#A000A0', 'stroke-width': 2 },
	'.marker-source': { stroke: '#A000A0', fill: '#A000A0', d: 'M 0 0 a 8 8 0 1 0 0 1' }
	}

    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.standard.Link.define('orm.ISA', {
    attrs: {
        line: {
			stroke: '#A000A0',
			'fill': '#A000A0',
            strokeWidth: 3,
			sourceMarker: {
               fill: '',
			   d: ''
            },
			targetMarker: { // hour hand
				'type': 'path',
				z:100000000000,
				'd': 'M 11 6 L 11 -6 L 0 0 z',
				stroke: '#A000A0',
				fill: '#A000A0',
			}

        }
    }
	}, {
    // inherit joint.shapes.standard.Link.markup
});

joint.shapes.standard.Link.define('orm.CustomLink', {
    attrs: {
        line: {
            stroke: '#333333',
            strokeWidth: 2,
            sourceMarker: {
               fill: 'yellow',
			   d: ''
            },
		    targetMarker: { // hour hand
				'type': 'path',
				z:100000000000,
				'd': 'M 8 0 a 6 6 0 1 0 0 1',
				stroke: '#A000A0',
				'display':'none',
				fill: '#A000A0',
			},
        }
    }
	}, {
    // inherit joint.shapes.standard.Link.markup
});

joint.shapes.standard.Link.define('orm.DashedLink', {
    attrs: {
        line: {
			stroke: '#a000a0',
			'stroke-dasharray': '6 4',
			'fill': '#a000a0',
            //stroke: '#333333',
            strokeWidth: 2,
			sourceMarker: {
               fill: 'yellow',
			   d: ''
            },
			 targetMarker: {
               fill: 'yellow',
			   d: ''
            },


        }
    }
	}, {
    // inherit joint.shapes.standard.Link.markup
});

joint.shapes.standard.Link.define('orm.DashedLinkArrow', {
    attrs: {
        line: {
			stroke: '#A000A0',
			'stroke-dasharray': '6 4',
			'fill': '#A000A0',
            //stroke: '#333333',
            strokeWidth: 2,
			sourceMarker: {
               fill: 'yellow',
			   d: ''
            },
			targetMarker: {
				stroke: '#A000A0',
				fill: '#A000A0',
				d: 'M 11 6 L 11 -6 L 0 0 z' }
		}
    }
	}, {
    // inherit joint.shapes.standard.Link.markup
});



